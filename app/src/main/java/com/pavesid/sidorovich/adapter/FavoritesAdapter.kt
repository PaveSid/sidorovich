package com.pavesid.sidorovich.adapter

import android.Manifest
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.ActionMode
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.extensions.dpToPx
import com.pavesid.sidorovich.model.App
import com.pavesid.sidorovich.model.Contact
import com.pavesid.sidorovich.ui.activities.MainActivity
import com.pavesid.sidorovich.utils.Utils
import com.pavesid.sidorovich.viewmodels.AppViewModel

class FavoritesAdapter(
    private val viewModel: AppViewModel,
    private var activity: MainActivity,
    private var gridWidth: Int = 0
) : BaseAdapter (activity) {

    private val margin = 8.dpToPx(activity.resources)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            Utils.CONTACT -> {
                val view = createView(gridWidth)

                ContactVH(view)
            }
            Utils.APPLICATION -> {
                val view = createView(gridWidth)

                ApplicationVH(view)
            }
            else -> {
//                val view =
                val view = createView(gridWidth)

                EmptyVH(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ContactVH -> {
                holder.bind(apps[position] as Contact)
            }
            is ApplicationVH -> {
                holder.bind(apps[position] as App)
            }
            is EmptyVH -> {
                holder.bind(apps[position])
            }
        }
    }

    inner class ApplicationVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view: ImageView = itemView.findViewById(R.id.viewDesktop)
        val text: TextView = itemView.findViewById(R.id.textDesktop)

        fun bind(app: App) {
            text.apply {
                layoutParams.width = gridWidth
                maxLines = 1
                text = app.name
            }

            view.apply {
                layoutParams.width = gridWidth
                layoutParams.height = gridWidth
                setImageDrawable(app.image)
//                background = app.image?.loadIcon(activity.packageManager)
            }

            itemView.setOnLongClickListener {
                if (activity.actionMode != null) {
                    return@setOnLongClickListener false
                }

                // Start the CAB using the ActionMode.Callback defined above
                activity.actionMode = activity.startActionMode(object : ActionMode.Callback {
                    // Called when the action mode is created; startActionMode() was called
                    override fun onCreateActionMode(mode: ActionMode, menu: Menu?): Boolean {
                        mode.menuInflater.inflate(R.menu.context_menu_bottom, menu)
                        return true
                    }

                    // Called each time the action mode is shown. Always called after onCreateActionMode, but
                    // may be called multiple times if the mode is invalidated.
                    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                        return false // Return false if nothing is done
                    }

                    // Called when the user selects a contextual menu item
                    override fun onActionItemClicked(
                        mode: ActionMode,
                        menuItem: MenuItem
                    ): Boolean {
                        return when (menuItem.itemId) {
                            R.id.delete -> {
                                //viewModel.deleteFavorite(app)
                                viewModel.cleanAppFromFavorites(app)
                                mode.finish() // Action picked, so close the CAB
                                true
                            }
                            else -> false
                        }
                    }

                    // Called when the user exits the action mode
                    override fun onDestroyActionMode(mode: ActionMode?) {
                        activity.actionMode = null
                    }
                })
                return@setOnLongClickListener true
            }

            itemView.setOnClickListener {
                if (app.entity != null) {
                    activity.startActivity(
                        activity.packageManager.getLaunchIntentForPackage(app.packageName)
                    )
                    viewModel.addPopular(app)
                    viewModel.modifyItem(app)
                    if (viewModel.getMode() == AppViewModel.SORT_COUNT) {
                        notifyDataSetChanged()
                    }
                }
            }
        }
    }

    inner class ContactVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view: ImageView = itemView.findViewById(R.id.viewDesktop)
        val text: TextView = itemView.findViewById(R.id.textDesktop)

        fun bind(item: Contact) {
            text.apply {
                layoutParams.width = gridWidth
                maxLines = 1

                this.text = item.name
            }
            view.apply {
                layoutParams.width = gridWidth
                layoutParams.height = gridWidth

                this.background =
                    BitmapDrawable(resources, retrieveContactPhoto(activity, item.number))
            }

            itemView.setOnLongClickListener {
                if (activity.actionMode != null) {
                    return@setOnLongClickListener false
                }

                // Start the CAB using the ActionMode.Callback defined above
                activity.actionMode = activity.startActionMode(object : ActionMode.Callback {
                    // Called when the action mode is created; startActionMode() was called
                    override fun onCreateActionMode(mode: ActionMode, menu: Menu?): Boolean {
                        mode.menuInflater.inflate(R.menu.context_menu_bottom, menu)
                        return true
                    }

                    // Called each time the action mode is shown. Always called after onCreateActionMode, but
                    // may be called multiple times if the mode is invalidated.
                    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                        return false // Return false if nothing is done
                    }

                    // Called when the user selects a contextual menu item
                    override fun onActionItemClicked(
                        mode: ActionMode,
                        menuItem: MenuItem
                    ): Boolean {
                        return when (menuItem.itemId) {
                            R.id.delete -> {
                                viewModel.cleanContactFromFavorites(item)
                                mode.finish() // Action picked, so close the CAB
                                true
                            }
                            else -> false
                        }
                    }

                    // Called when the user exits the action mode
                    override fun onDestroyActionMode(mode: ActionMode?) {
                        activity.actionMode = null
                    }
                })
                return@setOnLongClickListener true
            }

            itemView.setOnClickListener {

                activity.checkPermission(
                    Manifest.permission.CALL_PHONE,
                    MainActivity.CALL_PHONE_PERMISSION_CODE,
                    item.number
                )
            }
        }
    }

    inner class EmptyVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view: ImageView = itemView.findViewById(R.id.viewDesktop)
        val text: TextView = itemView.findViewById(R.id.textDesktop)

        fun bind(item: Any) {
            view.apply {
                layoutParams.width = gridWidth
                layoutParams.height = gridWidth

                this.background = getDefaultDrawable(activity, R.drawable.ic_add_box)
            }

            text.apply {
                layoutParams.width = gridWidth
                text = ""
            }

            itemView.setOnLongClickListener {
                activity.viewTag = adapterPosition//it.tag.toString().toInt()
                activity.checkPermission(
                    Manifest.permission.READ_CONTACTS,
                    MainActivity.READ_CONTACTS_PERMISSION_CODE,
                    ""
                )
                return@setOnLongClickListener true
            }

            itemView.setOnClickListener {
                Toast.makeText(
                    activity,
                    "You can add. Click on the necessary view",
                    Toast.LENGTH_SHORT
                )
                    .show()
                activity.favorites = adapterPosition//it.tag.toString().toInt()
            }
        }
    }

    private fun getDefaultDrawable(activity: MainActivity?, @DrawableRes drawable: Int): Drawable? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity?.resources?.getDrawable(
                drawable,
                activity.theme
            )
        } else {
            activity?.resources?.getDrawable(drawable)
        }
    }

    private fun createView(gridWidth: Int): LinearLayout {
        val parent = LinearLayout(activity)
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        params.setMargins(margin, margin, margin, margin)
        parent.layoutParams = params
        parent.orientation = LinearLayout.VERTICAL

        val imageView = ImageView(activity)
        imageView.layoutParams = LinearLayout.LayoutParams(gridWidth, gridWidth)
        imageView.id = R.id.viewDesktop

        val textView = TextView(activity)
        textView.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        textView.gravity = Gravity.CENTER
        textView.setPadding(0, margin, 0, 0)
        textView.textSize = 12F
        textView.id = R.id.textDesktop

        parent.addView(imageView)
        parent.addView(textView)

        return parent
    }
}