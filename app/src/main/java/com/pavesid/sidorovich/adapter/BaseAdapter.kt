package com.pavesid.sidorovich.adapter

import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.provider.ContactsContract
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.model.Desktop
import com.pavesid.sidorovich.ui.activities.MainActivity
import java.io.InputStream

abstract class BaseAdapter(
    private var activity: MainActivity
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    protected var apps: MutableList<out Desktop> = mutableListOf()

    override fun getItemCount(): Int = apps.size

    fun updateData(data: List<Desktop>) {

        val diffCallback = object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                apps[oldItemPosition].getDesktopName() == data[newItemPosition].getDesktopName()

            override fun getOldListSize() = apps.size

            override fun getNewListSize() = data.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                apps[oldItemPosition].hashCode() == data[newItemPosition].hashCode()
        }

        val diffResult = DiffUtil.calculateDiff(diffCallback)
        apps = data.toMutableList()
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemViewType(position: Int): Int {
        return apps[position].getDesktopType()
    }

    protected fun retrieveContactPhoto(context: Context, number: String?): Bitmap? {
        val contentResolver: ContentResolver = context.contentResolver
        var contactId: Long = 0
        val uri = Uri.withAppendedPath(
            ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
            Uri.encode(number)
        )
        val projection = arrayOf(
            ContactsContract.PhoneLookup.DISPLAY_NAME,
            ContactsContract.PhoneLookup._ID
        )
        val cursor: Cursor? = contentResolver.query(
            uri,
            projection,
            null,
            null,
            null
        )
        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId =
                    cursor.getLong(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID))
            }
            cursor.close()
        }

        var photo: Bitmap? = getBitmapFromVectorDrawable(R.drawable.ic_account_box)

        try {
            val inputStream: InputStream? =
                ContactsContract.Contacts.openContactPhotoInputStream(
                    context.contentResolver,
                    ContentUris.withAppendedId(
                        ContactsContract.Contacts.CONTENT_URI,
                        contactId
                    )
                )
            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream)
            }
            assert(inputStream != null)
            inputStream?.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return photo
    }

    private fun getBitmapFromVectorDrawable(@DrawableRes drawableId: Int): Bitmap? {
        var drawable: Drawable? =
            ContextCompat.getDrawable(activity, drawableId)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP && drawable != null) {
            drawable = DrawableCompat.wrap(drawable).mutate()
        }
        val bitmap = Bitmap.createBitmap(
            drawable?.intrinsicWidth ?: 0,
            drawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable?.setBounds(0, 0, canvas.width, canvas.height)
        drawable?.draw(canvas)
        return bitmap
    }
}