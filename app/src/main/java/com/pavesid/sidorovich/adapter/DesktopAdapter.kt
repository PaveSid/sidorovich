package com.pavesid.sidorovich.adapter

import android.Manifest
import android.graphics.drawable.BitmapDrawable
import android.view.ActionMode
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pavesid.sidorovich.LauncherApp
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.extensions.dpToPx
import com.pavesid.sidorovich.model.App
import com.pavesid.sidorovich.model.Contact
import com.pavesid.sidorovich.model.Desktop
import com.pavesid.sidorovich.model.Web
import com.pavesid.sidorovich.ui.fragments.ItemTouchHelperListener
import com.pavesid.sidorovich.ui.activities.MainActivity
import com.pavesid.sidorovich.utils.Utils
import com.pavesid.sidorovich.utils.Utils.APPLICATION
import com.pavesid.sidorovich.utils.Utils.CONTACT
import com.pavesid.sidorovich.utils.Utils.EMPTY
import com.pavesid.sidorovich.utils.Utils.WEB
import com.pavesid.sidorovich.viewmodels.AppViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.util.Collections

class DesktopAdapter(
    private val viewModel: AppViewModel,
    private var activity: MainActivity,
    private var gridWidth: Int = 0
) : BaseAdapter(activity), ItemTouchHelperListener {

//    var scrollDirection = ScrollDirection.DOWN

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            CONTACT -> {
                val view = createView(gridWidth)

                ContactVH(view)
            }
            APPLICATION -> {
                val view = createView(gridWidth)

                ApplicationVH(view)
            }
            EMPTY -> {
                val view = createView(gridWidth)

                EmptyVH(view)
            }
            WEB -> {
                val view = createView(gridWidth)
                WebVH(view)
            }
            else -> NotVH(
                View(activity)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ContactVH -> {
                holder.bind(apps[position] as Contact)
            }
            is ApplicationVH -> {
                holder.bind(apps[position] as App)
            }
            is WebVH -> {
                holder.bind(apps[position] as Web)
            }
            is EmptyVH -> {
                holder.bind(apps[position])
            }
        }
    }

    inner class ApplicationVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view: ImageView = itemView.findViewById(R.id.viewDesktop)
        val text: TextView = itemView.findViewById(R.id.textDesktop)

        fun bind(app: App) {
            view.setImageDrawable(app.image)
//            view.background = app.image?.loadIcon(activity.packageManager)
            text.text = app.name
            itemView.setOnLongClickListener {
                if (activity.actionMode != null) {
                    return@setOnLongClickListener false
                }

                // Start the CAB using the ActionMode.Callback defined above
                activity.actionMode =
                    itemView.startActionMode(object : ActionMode.Callback {
                        // Called when the action mode is created; startActionMode() was called
                        override fun onCreateActionMode(
                            mode: ActionMode,
                            menu: Menu?
                        ): Boolean {
                            mode.menuInflater.inflate(R.menu.context_menu_bottom, menu)
                            return true
                        }

                        // Called each time the action mode is shown. Always called after onCreateActionMode, but
                        // may be called multiple times if the mode is invalidated.
                        override fun onPrepareActionMode(
                            mode: ActionMode?,
                            menu: Menu?
                        ): Boolean {
                            return false // Return false if nothing is done
                        }

                        // Called when the user selects a contextual menu item
                        override fun onActionItemClicked(
                            mode: ActionMode,
                            menuItem: MenuItem
                        ): Boolean {
                            return when (menuItem.itemId) {
                                R.id.delete -> {
//                                    viewModel.deleteContactFromDesktop()
                                    viewModel.cleanApp(app)
                                    mode.finish() // Action picked, so close the CAB
                                    true
                                }
                                else -> false
                            }
                        }

                        // Called when the user exits the action mode
                        override fun onDestroyActionMode(mode: ActionMode?) {
                            activity.actionMode = null
                        }
                    })
                return@setOnLongClickListener true
            }

            itemView.setOnClickListener {
                itemView.context.startActivity(
                    itemView.context.packageManager.getLaunchIntentForPackage(app.packageName)
                )

                Utils.metricaOpenApp(app.name, Utils.GRID)

                viewModel.addPopular(app)
                viewModel.modifyItem(app)
                if (viewModel.getMode() == AppViewModel.SORT_COUNT) {
                    this@DesktopAdapter.notifyDataSetChanged()
                } else {
                    this@DesktopAdapter.notifyItemChanged(adapterPosition)
                }
            }
        }
    }

    inner class ContactVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view: ImageView = itemView.findViewById(R.id.viewDesktop)
        val text: TextView = itemView.findViewById(R.id.textDesktop)

        fun bind(item: Contact) {
            text.text = item.name
            view.background =
                BitmapDrawable(activity.resources, retrieveContactPhoto(activity, item.number))
            itemView.setOnLongClickListener {
                if (activity.actionMode != null) {
                    return@setOnLongClickListener false
                }

                // Start the CAB using the ActionMode.Callback defined above
                activity.actionMode = activity.startActionMode(object : ActionMode.Callback {
                    // Called when the action mode is created; startActionMode() was called
                    override fun onCreateActionMode(mode: ActionMode, menu: Menu?): Boolean {
                        mode.menuInflater.inflate(R.menu.context_menu_bottom, menu)
                        return true
                    }

                    // Called each time the action mode is shown. Always called after onCreateActionMode, but
                    // may be called multiple times if the mode is invalidated.
                    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                        return false // Return false if nothing is done
                    }

                    // Called when the user selects a contextual menu item
                    override fun onActionItemClicked(
                        mode: ActionMode,
                        menuItem: MenuItem
                    ): Boolean {
                        return when (menuItem.itemId) {
                            R.id.delete -> {
                                viewModel.cleanContact(item)
                                mode.finish() // Action picked, so close the CAB
                                true
                            }
                            else -> false
                        }
                    }

                    // Called when the user exits the action mode
                    override fun onDestroyActionMode(mode: ActionMode?) {
                        activity.actionMode = null
                    }
                })
                return@setOnLongClickListener true
            }

            itemView.setOnClickListener {

                activity.checkPermission(
                    Manifest.permission.CALL_PHONE,
                    MainActivity.CALL_PHONE_PERMISSION_CODE,
                    item.number
                )
            }
        }
    }

    inner class WebVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view: ImageView = itemView.findViewById(R.id.viewDesktop)
        val text: TextView = itemView.findViewById(R.id.textDesktop)

        fun bind(item: Web) {
            CoroutineScope(IO).launch {
                val bitmap = LauncherApp.cache?.getImageFromCache(item.name)
                val icon = if (bitmap == null) {
                    activity.loadDrawable(item.url)
                } else {
                    BitmapDrawable(activity.resources, bitmap)
                }
                CoroutineScope(Dispatchers.Main).launch {
                    view.background = icon
                }
            }

            text.text = item.name
            itemView.setOnClickListener {
                activity.openWebPage(item.url)
            }
            itemView.setOnLongClickListener {
                if (activity.actionMode != null) {
                    return@setOnLongClickListener false
                }

                // Start the CAB using the ActionMode.Callback defined above
                activity.actionMode = activity.startActionMode(object : ActionMode.Callback {
                    // Called when the action mode is created; startActionMode() was called
                    override fun onCreateActionMode(mode: ActionMode, menu: Menu?): Boolean {
                        mode.menuInflater.inflate(R.menu.context_menu_bottom, menu)
                        return true
                    }

                    // Called each time the action mode is shown. Always called after onCreateActionMode, but
                    // may be called multiple times if the mode is invalidated.
                    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                        return false // Return false if nothing is done
                    }

                    // Called when the user selects a contextual menu item
                    override fun onActionItemClicked(
                        mode: ActionMode,
                        menuItem: MenuItem
                    ): Boolean {
                        return when (menuItem.itemId) {
                            R.id.delete -> {
                                viewModel.cleanWeb(item)
                                mode.finish() // Action picked, so close the CAB
                                true
                            }
                            else -> false
                        }
                    }

                    // Called when the user exits the action mode
                    override fun onDestroyActionMode(mode: ActionMode?) {
                        activity.actionMode = null
                    }
                })
                return@setOnLongClickListener true
            }
        }
    }

    inner class EmptyVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Desktop) {
            itemView.setOnLongClickListener {
                if (activity.actionMode != null) {
                    return@setOnLongClickListener false
                }

                // Start the CAB using the ActionMode.Callback defined above
                activity.actionMode = activity.startActionMode(object : ActionMode.Callback {
                    // Called when the action mode is created; startActionMode() was called
                    override fun onCreateActionMode(mode: ActionMode, menu: Menu?): Boolean {
                        mode.menuInflater.inflate(R.menu.context_menu_bottom, menu)
                        return true
                    }

                    // Called each time the action mode is shown. Always called after onCreateActionMode, but
                    // may be called multiple times if the mode is invalidated.
                    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                        return false // Return false if nothing is done
                    }

                    // Called when the user selects a contextual menu item
                    override fun onActionItemClicked(
                        mode: ActionMode,
                        menuItem: MenuItem
                    ): Boolean {
                        return when (menuItem.itemId) {
                            R.id.delete -> {
                                viewModel.deleteFromDesktop(item)
                                mode.finish() // Action picked, so close the CAB
                                true
                            }
                            else -> false
                        }
                    }

                    // Called when the user exits the action mode
                    override fun onDestroyActionMode(mode: ActionMode?) {
                        activity.actionMode = null
                    }
                })
                return@setOnLongClickListener true
            }
        }
    }

    inner class NotVH(itemView: View) : RecyclerView.ViewHolder(itemView)

    var start = -1
    var end = -1

    override fun onItemMove(
        recyclerView: RecyclerView,
        fromPosition: Int,
        toPosition: Int
    ): Boolean {
        activity.actionMode?.finish()
        if (start == -1) {
            start = fromPosition
        }
        end = toPosition
//        if (fromPosition < toPosition) {
//            for (i in fromPosition until toPosition) {
//                Collections.swap(apps, start, i + 1)
////
////                val from = apps[i].getDesktopPosition()
////                val to = apps[i + 1].getDesktopPosition()
////                apps[i].setDesktopPosition(to)
////                apps[i + 1].setDesktopPosition(from)
//            }
//        } else {
//            for (i in fromPosition downTo toPosition + 1) {
//                Collections.swap(apps, start, i - 1)
////
////                val from = apps[i].getDesktopPosition()
////                val to = apps[i - 1].getDesktopPosition()
////                apps[i].setDesktopPosition(to)
////                apps[i - 1].setDesktopPosition(from)
//            }
//        }

//        notifyItemMoved(fromPosition, toPosition)
        return true
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        if (start != -1 && end != -1) {
            val startElem = apps[start]
            val endElem = apps[end]
            Collections.swap(apps, start, end)
//            notifyItemMoved(end, start)
//            notifyItemMoved(start, end)
            notifyItemChanged(start)
            notifyItemChanged(end)
//            notifyDataSetChanged()
            startElem.apply {
                setDesktopPosition(end)
                updatePosition(end, viewModel)
            }
            endElem.apply {
                setDesktopPosition(start)
                updatePosition(start, viewModel)
            }
            start = -1
            end = -1
        }
//        viewModel.changeDesktop()
//        for (app in apps) {
//            if (app.getDesktopPosition() == start) {
//                app.setDesktopPosition(end)
//                app.updatePosition(end, viewModel)
//            } else if (app.getDesktopPosition() == end) {
//                app.setDesktopPosition(start)
//                app.updatePosition(start, viewModel)
//            }
//        }
//        apps.forEach {
//            it.updatePosition(it.getDesktopPosition(), viewModel)
//        }
    }

    private fun createView(gridWidth: Int): LinearLayout {
        val parent = LinearLayout(activity)
        parent.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        parent.orientation = LinearLayout.VERTICAL

        val imageView = ImageView(activity)
        val params = LinearLayout.LayoutParams(gridWidth, gridWidth)
        imageView.layoutParams = params
        imageView.id = R.id.viewDesktop

        val textView = TextView(activity)
        textView.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        textView.gravity = Gravity.CENTER
        textView.setPadding(0, 8.dpToPx(activity.resources), 0, 0)
        textView.textSize = 12F
        textView.id = R.id.textDesktop

        parent.addView(imageView)
        parent.addView(textView)

        return parent
    }

    enum class ScrollDirection {
        UP, DOWN
    }
}