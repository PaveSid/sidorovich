package com.pavesid.sidorovich.adapter

import android.os.Build
import android.view.ActionMode
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.model.App
import com.pavesid.sidorovich.ui.activities.MainActivity
import com.pavesid.sidorovich.utils.Utils
import com.pavesid.sidorovich.utils.Utils.APPLICATION
import com.pavesid.sidorovich.utils.Utils.POPULAR_HEADER
import com.pavesid.sidorovich.utils.Utils.SINGLE_CAT
import com.pavesid.sidorovich.viewmodels.AppViewModel
import kotlinx.android.synthetic.main.item_for_list.view.avatar
import kotlinx.android.synthetic.main.item_for_list.view.text
import kotlinx.android.synthetic.main.item_for_list.view.title

class ItemAdapter(
    private val viewModel: AppViewModel,
    private var activity: MainActivity?,
    private val mode: Int = 0,
    private var gridWidth: Int = 0
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var scrollDirection = ScrollDirection.DOWN

    private var apps: MutableList<App?> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            APPLICATION,
            SINGLE_CAT -> when (mode) {
                0 -> ItemViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_for_list, parent, false)
                )
                1 -> {
                    val view = createView()

                    GridItemViewHolder(view)
                }
                else -> {
                    val view = createView()

                    GridItemViewHolder(view)
                }
            }
            POPULAR_HEADER -> {
                    val view =
                        LayoutInflater.from(parent.context)
                            .inflate(R.layout.popular_header, parent, false)
                HeaderViewHolder(view)
            }
            else -> {
                    val view =
                        LayoutInflater.from(parent.context)
                            .inflate(R.layout.app_header, parent, false)
                HeaderViewHolder(view)
            }
        }

    override fun getItemCount() = apps.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemViewHolder -> {
                val item = apps[position]
                if (item != null) {
                    holder.bind(item)
                }
            }
            is GridItemViewHolder -> {
                val item = apps[position]
                if (item != null) {
                    holder.bind(item)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return apps[position]?.type ?: 0
    }

    fun updateData(data: List<App?>) {

        val diffCallback = object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                apps[oldItemPosition]?.packageName == data[newItemPosition]?.packageName

            override fun getOldListSize() = apps.size

            override fun getNewListSize() = data.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                apps[oldItemPosition].hashCode() == data[newItemPosition].hashCode()
        }

        val diffResult = DiffUtil.calculateDiff(diffCallback)
        apps = data.toMutableList()
        diffResult.dispatchUpdatesTo(this)
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(app: App) {
            when (app.type) {
                APPLICATION -> {
                    itemView.avatar.setImageDrawable(app.image)//?.loadIcon(activity?.packageManager))
                    itemView.text.text =
                        app.entity?.count.toString()// item.time.toDate()
                    itemView.title.text = app.name
                    itemView.setOnClickListener {
                        if (activity?.peek == false) {
                            itemView.context.startActivity(
                                itemView.context.packageManager.getLaunchIntentForPackage(app.packageName)
                            )
                            viewModel.addPopular(app)
                            viewModel.modifyItem(app)

                            Utils.metricaOpenApp(app.name, Utils.LIST)

                            if (viewModel.getMode() == AppViewModel.SORT_COUNT) {
                                this@ItemAdapter.notifyDataSetChanged()
                            } else {
                                this@ItemAdapter.notifyItemChanged(adapterPosition)
                            }
                        } else if (app.entity?.position == -1) {
                            viewModel.addToDesktop(app)
                            activity?.peekAppClose()
                        } else {
                            Toast.makeText(activity, "Это приложение уже на столе", Toast.LENGTH_SHORT).show()
                        }
                    }
                    itemView.setOnLongClickListener {
                        if (activity?.actionMode != null) {
                            return@setOnLongClickListener false
                        }

                        // Start the CAB using the ActionMode.Callback defined above
                        activity?.actionMode =
                            itemView.startActionMode(object : ActionMode.Callback {
                                // Called when the action mode is created; startActionMode() was called
                                override fun onCreateActionMode(
                                    mode: ActionMode,
                                    menu: Menu?
                                ): Boolean {
                                    mode.menuInflater.inflate(R.menu.context_menu, menu)
                                    return true
                                }

                                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                                // may be called multiple times if the mode is invalidated.
                                override fun onPrepareActionMode(
                                    mode: ActionMode?,
                                    menu: Menu?
                                ): Boolean {
                                    return false // Return false if nothing is done
                                }

                                // Called when the user selects a contextual menu item
                                override fun onActionItemClicked(
                                    mode: ActionMode,
                                    menuItem: MenuItem
                                ): Boolean {
                                    return when (menuItem.itemId) {
                                        R.id.delete -> {
                                            activity?.deleteApp(app.packageName)
                                            mode.finish() // Action picked, so close the CAB
                                            Utils.metricaDelete(app.name)
                                            true
                                        }
                                        R.id.quantity -> {
                                            Toast.makeText(
                                                activity,
                                                "${app.entity?.count}",
                                                Toast.LENGTH_LONG
                                            )
                                                .show()
                                            mode.finish() // Action picked, so close the CAB
                                            Utils.metricaQuantity(app.name)
                                            true
                                        }
                                        R.id.settings -> {
                                            activity?.openSettings(app.packageName)
                                            mode.finish() // Action picked, so close the CAB
                                            Utils.metricaOpenSettings(app.name)
                                            true
                                        }
                                        else -> false
                                    }
                                }

                                // Called when the user exits the action mode
                                override fun onDestroyActionMode(mode: ActionMode?) {
                                    activity?.actionMode = null
                                }
                            })
                        return@setOnLongClickListener true
                    }
                }
                SINGLE_CAT -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        itemView.avatar.setImageDrawable(itemView.resources.getDrawable(R.drawable.cat, itemView.context.theme))
                    } else {
                        itemView.avatar.setImageDrawable(itemView.resources.getDrawable(R.drawable.cat))
                    }
                    itemView.title.text = itemView.context.getString(R.string.where_everybody)
                }
                else -> {
                }
            }
        }
    }

    inner class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class GridItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val view: ImageView = itemView.findViewById(R.id.viewDesktop)
        val text: TextView = itemView.findViewById(R.id.textDesktop)

        fun bind(app: App) {
            when (app.type) {
                APPLICATION -> {
                    view.setImageDrawable(app.image)
//                    view.background = app.image?.loadIcon(activity?.packageManager)
                    text.text = app.name
                    itemView.setOnLongClickListener {
                        if (activity?.actionMode != null) {
                            return@setOnLongClickListener false
                        }

                        // Start the CAB using the ActionMode.Callback defined above
                        activity?.actionMode =
                            itemView.startActionMode(object : ActionMode.Callback {
                                // Called when the action mode is created; startActionMode() was called
                                override fun onCreateActionMode(
                                    mode: ActionMode,
                                    menu: Menu?
                                ): Boolean {
                                    mode.menuInflater.inflate(R.menu.context_menu, menu)
                                    return true
                                }

                                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                                // may be called multiple times if the mode is invalidated.
                                override fun onPrepareActionMode(
                                    mode: ActionMode?,
                                    menu: Menu?
                                ): Boolean {
                                    return false // Return false if nothing is done
                                }

                                // Called when the user selects a contextual menu item
                                override fun onActionItemClicked(
                                    mode: ActionMode,
                                    menuItem: MenuItem
                                ): Boolean {
                                    return when (menuItem.itemId) {
                                        R.id.delete -> {
                                            activity?.deleteApp(app.packageName)
                                            mode.finish() // Action picked, so close the CAB
                                            Utils.metricaDelete(app.name)
                                            true
                                        }
                                        R.id.quantity -> {
                                            Toast.makeText(
                                                activity,
                                                "${app.entity?.count}",
                                                Toast.LENGTH_LONG
                                            )
                                                .show()
                                            mode.finish() // Action picked, so close the CAB
                                            Utils.metricaQuantity(app.name)
                                            true
                                        }
                                        R.id.settings -> {
                                            activity?.openSettings(app.packageName)
                                            mode.finish() // Action picked, so close the CAB
                                            Utils.metricaOpenSettings(app.name)
                                            true
                                        }
                                        else -> false
                                    }
                                }

                                // Called when the user exits the action mode
                                override fun onDestroyActionMode(mode: ActionMode?) {
                                    activity?.actionMode = null
                                }
                            })
                        return@setOnLongClickListener true
                    }

                    itemView.setOnClickListener {
                        val fav = activity?.favorites ?: -1
                        if (fav != -1) {

                            Utils.metricaToFavorite(app.name, fav)
                            viewModel.addAppToFavorites(app, fav)
                            activity?.favorites = -1
                        } else {
                            itemView.context.startActivity(
                                itemView.context.packageManager.getLaunchIntentForPackage(app.packageName)
                            )

                            Utils.metricaOpenApp(app.name, Utils.GRID)

                            viewModel.addPopular(app)
                            viewModel.modifyItem(app)
                            if (viewModel.getMode() == AppViewModel.SORT_COUNT) {
                                this@ItemAdapter.notifyDataSetChanged()
                            } else {
                                this@ItemAdapter.notifyItemChanged(adapterPosition)
                            }
                        }
                    }
                }
                SINGLE_CAT -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        view.background = itemView.resources.getDrawable(R.drawable.cat, itemView.context.theme)
                    } else {
                        view.background = itemView.resources.getDrawable(R.drawable.cat)
                    }
                    text.text = itemView.context.getString(R.string.where_everybody)
                }
            }
        }
    }

    private fun createView(): LinearLayout {
        val parent = LinearLayout(activity)
        parent.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        parent.orientation = LinearLayout.VERTICAL

        val imageView = ImageView(activity)
        imageView.layoutParams = LinearLayout.LayoutParams(gridWidth, gridWidth)
        imageView.id = R.id.viewDesktop

        val textView = TextView(activity)
        textView.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        textView.gravity = Gravity.CENTER
        textView.setPadding(0, 8, 0, 0)
        textView.textSize = 12F
        textView.id = R.id.textDesktop

        parent.addView(imageView)
        parent.addView(textView)

        return parent
    }

    enum class ScrollDirection {
        UP, DOWN
    }
}