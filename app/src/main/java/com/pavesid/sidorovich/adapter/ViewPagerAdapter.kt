package com.pavesid.sidorovich.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.pavesid.sidorovich.ui.fragments.welcome.DescriptionFragment
import com.pavesid.sidorovich.ui.fragments.welcome.LayoutSelectionFragment
import com.pavesid.sidorovich.ui.fragments.welcome.ThemeSelectionFragment
import com.pavesid.sidorovich.ui.fragments.welcome.WelcomeFragment
import com.pavesid.sidorovich.ui.fragments.welcome.GoToLauncherFragment

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val fragments = arrayOf(
        WelcomeFragment.newInstance(),
        DescriptionFragment.newInstance(),
        LayoutSelectionFragment.newInstance(),
        ThemeSelectionFragment.newInstance(),
        GoToLauncherFragment.newInstance()
    )

//    private val title = arrayOf(
//        "Welcome",
//        "Description",
//        "LayoutSelection",
//        "ThemeSelection",
//        "Go to Launcher"
//    )

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int = fragments.size

//    override fun getPageTitle(position: Int): CharSequence? {
//        return title[position]
//    }
}