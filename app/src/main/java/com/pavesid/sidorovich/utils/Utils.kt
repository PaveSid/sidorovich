package com.pavesid.sidorovich.utils

import android.content.res.Resources
import android.util.TypedValue
import androidx.annotation.AttrRes
import com.yandex.metrica.YandexMetrica
import java.lang.Exception

object Utils {

    const val GRID = "grid"
    const val LIST = "list"
    const val SETTINGS = "settings"

    const val APPLICATION = 0
    const val POPULAR_HEADER = 1
    const val APPLICATIONS_HEADER = 2
    const val SINGLE_CAT = 3
    const val CONTACT = 4
    const val WEB = 5
    const val EMPTY = 6

    fun metricaDelete(name: String) {
        val eventParameters = "{\"name\":\"$name\"}"
        YandexMetrica.reportEvent("Delete", eventParameters)
    }

    fun metricaOpenSettings(name: String) {
        val eventParameters = "{\"name\":\"$name\"}"
        YandexMetrica.reportEvent("Open settings", eventParameters)
    }

    fun metricaQuantity(name: String) {
        val eventParameters = "{\"name\":\"$name\"}"
        YandexMetrica.reportEvent("Looked quantity", eventParameters)
    }

    fun metricaOpenApp(name: String, from: String) {
        val eventParameters = "{\"name\":\"$name\", \"from\":\"$from\"}"
        YandexMetrica.reportEvent("Open app", eventParameters)
    }

    fun metricaToFavorite(name: String, to: Int) {
        val eventParameters = "{\"name\":\"$name\", \"to\":\"$to\"}"
        YandexMetrica.reportEvent("Add to favorite", eventParameters)
    }

    fun metricaOpenFragment(name: String) {
        val eventParameters = "{\"fragment\":\"$name\"}"
        YandexMetrica.reportEvent("Open fragment", eventParameters)
    }

    fun metricaCloseFragment(name: String) {
        val eventParameters = "{\"fragment\":\"$name\"}"
        YandexMetrica.reportEvent("Close fragment", eventParameters)
    }

    fun metricaError() {
        YandexMetrica.reportError("Much items", null)
    }

    fun metricaErrorFavorites() {
        YandexMetrica.reportError("Wrong favorites", null)
    }

    fun metricaErrorNetwork(e: Exception) {
        YandexMetrica.reportError("Network", e)
    }

    fun metricaErrorWithCursor(error: Throwable) {
        YandexMetrica.reportError("Cursor", error)
    }

    /**
     * Преобразует Attr формат в Color
     */
    fun getColorFromAttr(
        @AttrRes attrColor: Int,
        theme: Resources.Theme,
        typedValue: TypedValue = TypedValue(),
        resolveRefs: Boolean = true
    ): Int {
        theme.resolveAttribute(attrColor, typedValue, resolveRefs)
        return typedValue.data
    }
}