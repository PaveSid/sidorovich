package com.pavesid.sidorovich.utils

import androidx.room.TypeConverter

class Converter {

    companion object {

        @TypeConverter
        @JvmStatic
        fun fromList(dates: MutableList<Long>): String {
            val buffer = StringBuffer()
            for (date in dates) {
                buffer.append(date).append(",")
            }
            if (buffer.isNotEmpty()) {
                buffer.deleteCharAt(buffer.length - 1)
            }
            return buffer.toString()
        }

        @TypeConverter
        @JvmStatic
        fun toList(dates: String): MutableList<Long> {
            return if (dates == "") {
                mutableListOf()
            } else {
                val str = dates.split(",")
                val listLong = mutableListOf<Long>()
                for (list in str) {
                    listLong.add(list.toLong())
                }
                listLong
            }
        }
    }
}