package com.pavesid.sidorovich.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

internal class ImageSaver private constructor() {
    fun saveImage(
        context: Context,
        bitmap: Bitmap,
        fileName: String
    ) {
        var fileOutputStream: FileOutputStream? = null
        try {
            fileOutputStream = FileOutputStream(createFile(context, fileName))
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fileOutputStream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun createFile(
        context: Context,
        fileName: String
    ): File {
        val directory = context.getDir(
            DIRECTORY_NAME,
            Context.MODE_PRIVATE
        )
        return File(directory, fileName)
    }

    fun loadImage(
        context: Context,
        fileName: String
    ): Bitmap? {
        var inputStream: FileInputStream? = null
        try {
            inputStream = FileInputStream(createFile(context, fileName))
            return BitmapFactory.decodeStream(inputStream)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                inputStream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return null
    }

    companion object {
        private val mLock = Any()

        @Volatile
        private var sInstance: ImageSaver? = null
        private const val DIRECTORY_NAME = "images"
        val instance: ImageSaver?
            get() {
                if (null == sInstance) {
                    synchronized(mLock) {
                        if (null == sInstance) {
                            sInstance =
                                ImageSaver()
                        }
                    }
                }
                return sInstance
            }
    }
}