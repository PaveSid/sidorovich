package com.pavesid.sidorovich.utils

import android.graphics.Bitmap
import android.util.Log
import androidx.collection.LruCache

class ImagesCache {
    private lateinit var memoryCache: LruCache<String, Bitmap>
    fun initializeCache() {
        val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()
        val cacheSize = maxMemory / 8
        Log.i("M_ImagesCache", "cache size = $cacheSize")

        memoryCache = object : LruCache<String, Bitmap>(cacheSize) {

            override fun sizeOf(key: String, bitmap: Bitmap): Int {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.byteCount / 1024
            }
        }
    }

    fun addImageToCache(key: String, value: Bitmap) {
//        val cache = memoryCache as LruCache<String, Bitmap>
        if (memoryCache.get(key) == null) {
            memoryCache.put(key, value)
        }
    }

    fun getImageFromCache(key: String): Bitmap? {
        return memoryCache.get(key)
    }

    fun removeImageFromCache(key: String) {
        memoryCache.remove(key)
    }

    fun clearCache() {
        memoryCache.evictAll()
    }

    companion object {
        private var cache: ImagesCache? = null
        val instance: ImagesCache?
            get() {
                if (cache == null) {
                    cache = ImagesCache()
                }
                return cache
            }
    }
}