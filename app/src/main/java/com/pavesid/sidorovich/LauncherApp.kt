package com.pavesid.sidorovich

import android.app.ActivityManager
import android.app.AlarmManager
import android.app.Application
import android.app.Service
import android.content.Context
import android.os.Process
import androidx.core.content.ContextCompat
import com.pavesid.sidorovich.utils.ImagesCache
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import com.yandex.metrica.push.YandexMetricaPush

class LauncherApp : Application() {
    companion object {
        private lateinit var instance: LauncherApp
        private lateinit var alarmManager: AlarmManager
        private const val API_key = "33811293-7f18-41fb-bfc4-7ff1331c1161"
        val cache = ImagesCache.instance

        fun applicationContext(): Context = instance.applicationContext

        fun getAlarmManager(): AlarmManager = alarmManager
    }

    init {
        instance = this
        cache?.initializeCache()
    }

    override fun onCreate() {
        super.onCreate()
        alarmManager = getSystemService(Service.ALARM_SERVICE) as AlarmManager
        // Creating an extended library configuration.
        val config: YandexMetricaConfig = YandexMetricaConfig.newConfigBuilder(API_key)
            .withLogs()
            .withCrashReporting(true)
            .build()
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(applicationContext, config)
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(this)
        if (isInMainProcess())
            YandexMetricaPush.init(applicationContext)
    }

    private fun isInMainProcess(): Boolean {
        val myPid = Process.myPid()
        val activityManager =
            ContextCompat.getSystemService(applicationContext, ActivityManager::class.java)
        return activityManager != null && activityManager.runningAppProcesses.any { process ->
            process.pid == myPid && process.processName == BuildConfig.APPLICATION_ID
        }
    }
}