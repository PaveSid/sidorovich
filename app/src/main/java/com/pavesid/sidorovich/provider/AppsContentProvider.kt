package com.pavesid.sidorovich.provider

import android.content.ContentProvider
import android.content.ContentResolver
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import com.pavesid.sidorovich.data.entityDB.AppsDao
import com.pavesid.sidorovich.data.MyRoomDatabase
import com.pavesid.sidorovich.model.AppEntity.Companion.COUNT
import com.pavesid.sidorovich.model.AppEntity.Companion.PACKAGE_NAME

class AppsContentProvider : ContentProvider() {

    companion object {

        private const val AUTHORITY = "com.pavesid.sidorovich.all_apps"
        private const val APPS_LIST = 1
        private const val LAST = 2
        private const val USER = 3

        private val URI_MATCHER: UriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        init {
            URI_MATCHER.addURI(AUTHORITY, "apps", APPS_LIST)
            URI_MATCHER.addURI(AUTHORITY, "last", LAST)
            URI_MATCHER.addURI(AUTHORITY, "user", USER)
        }
    }

    override fun onCreate(): Boolean = true

    override fun insert(uri: Uri, values: ContentValues): Uri? {
//        val matchedUri = URI_MATCHER.match(uri)
//        if (matchedUri == APPS_LIST) {
//            val id = AppsRoomDatabase.getDatabase(context).appsDao()
//                .insert(ItemEntity.fromContentValues(values))
//        }
        return null
    }

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {
        if (context == null)
            return null
        val db = MyRoomDatabase.getDatabase(context!!)
        return when (URI_MATCHER.match(uri)) {
            APPS_LIST -> db.appsDao().getAllRunningApps()
            LAST -> db.appsDao().getLast()
            USER -> db.userDao().getUserCursor()
            else -> throw IllegalArgumentException("Unsupported uri: $uri")
        }
    }

    override fun update(
        uri: Uri,
        values: ContentValues,
        selection: String?,
        selectionArgs: Array<String>?
    ): Int {
        if (context == null)
            return 0
        val apps: AppsDao = MyRoomDatabase.getDatabase(context!!).appsDao()
        when (URI_MATCHER.match(uri)) {
            APPS_LIST -> {
                val packageN = values.getAsString(PACKAGE_NAME)
                val count = values.getAsInteger(COUNT)
                return apps.updateFromMain(
                    count,
                    packageN
                )
            }
            else -> throw IllegalArgumentException("Unsupported URI for insertion: $uri")
        }
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        return 0
    }

    override fun getType(uri: Uri): String? {
        return when (URI_MATCHER.match(uri)) {
            APPS_LIST -> ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.$AUTHORITY.provider_list"
            LAST -> ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.$AUTHORITY.provider_last"
            USER -> ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.$AUTHORITY.provider_user"
            else -> throw IllegalArgumentException("Unsupported URI: $uri")
        }
    }
}