package com.pavesid.sidorovich.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.preference.PreferenceManager
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.HINT_PROFILE
import com.yandex.metrica.push.YandexMetricaPush

class SilentPushReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val string = intent.getStringExtra(YandexMetricaPush.EXTRA_PAYLOAD)
        if (string == "true") {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context.applicationContext)
            prefs.edit().apply {
                putBoolean(HINT_PROFILE, true)
                apply()
            }
        }
    }
}