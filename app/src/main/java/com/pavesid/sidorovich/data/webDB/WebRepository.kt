package com.pavesid.sidorovich.data.webDB

import com.pavesid.sidorovich.model.Web

class WebRepository(private val webDao: WebDao) {
    suspend fun loadWebs(): List<Web> = webDao.getWebs()

    suspend fun insertWeb(web: Web) {
        webDao.insert(web)
    }

    suspend fun updateWeb(web: Web) = webDao.update(web)

    suspend fun deleteWeb(web: Web) = webDao.delete(web)
}