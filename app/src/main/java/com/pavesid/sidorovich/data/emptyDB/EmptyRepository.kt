package com.pavesid.sidorovich.data.emptyDB

import com.pavesid.sidorovich.model.Empty

class EmptyRepository(private val emptyDao: EmptyDao) {
    suspend fun loadEmpties(): List<Empty> = emptyDao.getEmptiesDesktop()

    suspend fun loadEmptiesFavorites(): List<Empty> = emptyDao.getEmptiesFavorites()

    suspend fun updateEmpty(empty: Empty?) {
        emptyDao.update(empty)
    }

    suspend fun deleteEmpty(empty: Empty?) {
        emptyDao.delete(empty)
    }

    suspend fun addEmpty(empty: Empty?) {
        emptyDao.insert(empty)
    }
}