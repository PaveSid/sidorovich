package com.pavesid.sidorovich.data.entityDB

import android.database.Cursor
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import androidx.room.Delete
import androidx.room.OnConflictStrategy
import com.pavesid.sidorovich.model.AppEntity

@Dao
interface AppsDao {

    @Query("SELECT * from ${AppEntity.ENTITY_TABLE_NAME}")
    suspend fun getApps(): List<AppEntity>

    @Query ("SELECT * from ${AppEntity.ENTITY_TABLE_NAME} where ${AppEntity.IS_FAVORITE} != -1")
    suspend fun getFavorites(): List<AppEntity>

    @Update
    suspend fun update(app: AppEntity?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(app: AppEntity?): Long

    @Query ("delete from ${AppEntity.ENTITY_TABLE_NAME}")
    suspend fun deleteAll()

    @Delete
    suspend fun delete(app: AppEntity?)

    //For provider
    @Query ("SELECT ${AppEntity.NAME}, ${AppEntity.PACKAGE_NAME}, ${AppEntity.COUNT} from ${AppEntity.ENTITY_TABLE_NAME} where ${AppEntity.COUNT} > 0")
    fun getAllRunningApps(): Cursor

    @Query ("SELECT ${AppEntity.NAME}, ${AppEntity.PACKAGE_NAME}, ${AppEntity.COUNT} from ${AppEntity.ENTITY_TABLE_NAME} where ${AppEntity.LAST} = (SELECT MAX(${AppEntity.LAST}) FROM ${AppEntity.ENTITY_TABLE_NAME}) and ${AppEntity.COUNT} > 0")
    fun getLast(): Cursor

    @Query ("SELECT * from ${AppEntity.ENTITY_TABLE_NAME}")
    fun getAllApps(): Cursor

    @Query ("UPDATE ${AppEntity.ENTITY_TABLE_NAME} SET ${AppEntity.COUNT} = :count WHERE ${AppEntity.PACKAGE_NAME} = :packageN ")
    fun updateFromMain(count: Int?, packageN: String?): Int
}