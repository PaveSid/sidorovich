package com.pavesid.sidorovich.data.contactDB

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import androidx.room.Delete
import androidx.room.OnConflictStrategy
import com.pavesid.sidorovich.model.Contact

@Dao
interface ContactDao {

    @Query("SELECT * from ${Contact.NUMBER_PHONE_TABLE_NAME} where ${Contact.IS_FAVORITE} != -1")
    suspend fun getFavoritesContact(): List<Contact>

    @Query("SELECT * from ${Contact.NUMBER_PHONE_TABLE_NAME} where ${Contact.POSITION} != -1")
    suspend fun getDesktopContact(): List<Contact>

    @Query("SELECT * from ${Contact.NUMBER_PHONE_TABLE_NAME}")
    suspend fun getContacts(): List<Contact>

    @Update
    suspend fun update(item: Contact?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: Contact?): Long

    @Query ("delete from ${Contact.NUMBER_PHONE_TABLE_NAME}")
    suspend fun deleteAll()

    @Delete
    suspend fun delete(item: Contact?)
}