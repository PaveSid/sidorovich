package com.pavesid.sidorovich.data.entityDB

import com.pavesid.sidorovich.model.AppEntity

class AppsRepository(private val appsDao: AppsDao) {
    suspend fun loadItems(): List<AppEntity> = appsDao.getApps()

    suspend fun loadFavorites(): List<AppEntity> = appsDao.getFavorites()

    suspend fun updateItem(app: AppEntity?) {
        appsDao.update(app)
    }

    suspend fun deleteItem(app: AppEntity?) {
        appsDao.delete(app)
    }

    suspend fun addItem(app: AppEntity?) {
        appsDao.insert(app)
    }
}