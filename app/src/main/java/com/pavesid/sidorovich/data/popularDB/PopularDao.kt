package com.pavesid.sidorovich.data.popularDB

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.pavesid.sidorovich.model.PopularApp

@Dao
interface PopularDao {

    @Query("SELECT * from ${PopularApp.POPULARS_TABLE_NAME} where ${PopularApp.DATE}  > :time")
    suspend fun getPopulars(time: Long): List<PopularApp>

    @Insert
    suspend fun insert(item: PopularApp): Long

    @Query("delete from ${PopularApp.POPULARS_TABLE_NAME} where ${PopularApp.PACKAGE_NAME}  = :packageName")
    suspend fun deletePopular(packageName: String?)
}