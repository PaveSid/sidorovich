package com.pavesid.sidorovich.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.pavesid.sidorovich.data.entityDB.AppsDao
import com.pavesid.sidorovich.data.popularDB.PopularDao
import com.pavesid.sidorovich.data.contactDB.ContactDao
import com.pavesid.sidorovich.data.emptyDB.EmptyDao
import com.pavesid.sidorovich.data.userDB.UserDao
import com.pavesid.sidorovich.data.webDB.WebDao
import com.pavesid.sidorovich.model.PopularApp
import com.pavesid.sidorovich.model.AppEntity
import com.pavesid.sidorovich.model.Contact
import com.pavesid.sidorovich.model.Empty
import com.pavesid.sidorovich.model.User
import com.pavesid.sidorovich.model.Web
import com.pavesid.sidorovich.utils.Converter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(
    entities = [AppEntity::class, User::class, Contact::class, PopularApp::class, Web::class, Empty::class],
    version = 4
)
@TypeConverters(Converter::class)
abstract class MyRoomDatabase : RoomDatabase() {
    abstract fun appsDao(): AppsDao
    abstract fun userDao(): UserDao
    abstract fun contactDao(): ContactDao
    abstract fun popularDao(): PopularDao
    abstract fun webDao(): WebDao
    abstract fun emptyDao(): EmptyDao

    companion object {
        private const val DB_NAME = "apps"

        private lateinit var ctx: Context

        //Singleton
        @Volatile
        private var INSTANCE: MyRoomDatabase? = null

        fun getDatabase(context: Context): MyRoomDatabase {
            ctx = context
            val tempInstance =
                INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MyRoomDatabase::class.java,
                    DB_NAME
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(DatabaseCallback())
                    .build()
                INSTANCE = instance
                return instance
            }
        }

        class DatabaseCallback : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                INSTANCE?.let {
                    CoroutineScope(Dispatchers.IO).launch {
                        it.emptyDao().insert(Empty(isFavorite = 0))
                        it.emptyDao().insert(Empty(isFavorite = 1))
                        it.emptyDao().insert(Empty(isFavorite = 2))
                        it.emptyDao().insert(Empty(isFavorite = 3))
                    }
                }
            }
        }
    }
}