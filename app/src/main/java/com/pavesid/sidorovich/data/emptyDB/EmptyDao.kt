package com.pavesid.sidorovich.data.emptyDB

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import androidx.room.Delete
import androidx.room.OnConflictStrategy
import com.pavesid.sidorovich.model.Empty

@Dao
interface EmptyDao {

    @Query("SELECT * from ${Empty.EMPTY_TABLE_NAME} where ${Empty.POSITION} != -1")
    suspend fun getEmptiesDesktop(): List<Empty>

    @Query("SELECT * from ${Empty.EMPTY_TABLE_NAME} where ${Empty.IS_FAVORITE} != -1")
    suspend fun getEmptiesFavorites(): List<Empty>

    @Update
    suspend fun update(empty: Empty?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(empty: Empty?): Long

    @Query ("delete from ${Empty.EMPTY_TABLE_NAME}")
    suspend fun deleteAll()

    @Delete
    suspend fun delete(empty: Empty?)
}