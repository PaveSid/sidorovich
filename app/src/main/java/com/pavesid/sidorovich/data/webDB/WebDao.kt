package com.pavesid.sidorovich.data.webDB

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import androidx.room.Delete
import androidx.room.OnConflictStrategy
import com.pavesid.sidorovich.model.Web

@Dao
interface WebDao {

    @Query("SELECT * from ${Web.WEB_TABLE_NAME}")
    suspend fun getWebs(): List<Web>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(web: Web): Long

    @Update
    suspend fun update(web: Web)

    @Query ("delete from ${Web.WEB_TABLE_NAME}")
    suspend fun deleteAll()

    @Delete
    suspend fun delete(web: Web)
}