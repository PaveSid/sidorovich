package com.pavesid.sidorovich.data.popularDB

import com.pavesid.sidorovich.model.PopularApp

class PopularRepository(private val launchDao: PopularDao) {
    suspend fun loadPopulars(time: Long): List<PopularApp> = launchDao.getPopulars(time)

    suspend fun deletePopular(packageName: String?) = launchDao.deletePopular(packageName)

    suspend fun addPopular(launch: PopularApp) = launchDao.insert(launch)
}