package com.pavesid.sidorovich.data.userDB

import android.database.Cursor
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import androidx.room.Delete
import androidx.room.OnConflictStrategy
import com.pavesid.sidorovich.model.User

@Dao
interface UserDao {

    @Query("SELECT * from ${User.USER_TABLE_NAME}")
    suspend fun getUser(): User?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: User): Long

    @Update
    suspend fun update(user: User)

    @Query ("delete from ${User.USER_TABLE_NAME}")
    suspend fun deleteAll()

    @Delete
    suspend fun delete(user: User)

    //For provider
    @Query ("SELECT * from ${User.USER_TABLE_NAME}")
    fun getUserCursor(): Cursor
}