package com.pavesid.sidorovich.data.contactDB

import com.pavesid.sidorovich.model.Contact

class ContactRepository(private val numberDao: ContactDao) {
    suspend fun loadFavoritesContacts(): List<Contact> = numberDao.getFavoritesContact()

    suspend fun loadDesktopContacts(): List<Contact> = numberDao.getDesktopContact()

    suspend fun loadContacts(): List<Contact> = numberDao.getContacts()

    suspend fun updateNumber(number: Contact?) {
        numberDao.update(number)
    }

    suspend fun deleteNumber(number: Contact?) {
        numberDao.delete(number)
    }

    suspend fun addNumber(number: Contact?) {
        numberDao.insert(number)
    }
}