package com.pavesid.sidorovich.data.userDB

import com.pavesid.sidorovich.model.User

class UserRepository(private val userDao: UserDao) {
    suspend fun loadUser(): User? = userDao.getUser()

    suspend fun insertUser(user: User) {
        userDao.deleteAll()
        userDao.insert(user)
    }

    suspend fun updateUser(user: User) = userDao.update(user)

    suspend fun deleteUser(user: User) = userDao.delete(user)
}