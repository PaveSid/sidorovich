package com.pavesid.sidorovich.extensions

import java.text.DateFormat

fun Long.toDate(): String {
    val df: DateFormat = DateFormat.getDateTimeInstance()//SimpleDateFormat("dd:MM:yy:HH:mm:ss")
    return df.format(this)
}