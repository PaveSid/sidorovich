package com.pavesid.sidorovich.extensions

import android.content.res.Resources

fun Int.dpToPx(resources: Resources): Int {
    return (this * resources.displayMetrics.density + 0.5f).toInt()
}

fun Int.spToPx(resources: Resources): Float {
    return this * resources.displayMetrics.scaledDensity
}