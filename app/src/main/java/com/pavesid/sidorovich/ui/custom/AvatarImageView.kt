package com.pavesid.sidorovich.ui.custom

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Shader
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.annotation.ColorInt
import androidx.core.graphics.drawable.toBitmap
import androidx.core.graphics.toRectF
import com.pavesid.sidorovich.R
import kotlin.math.truncate

class AvatarImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : androidx.appcompat.widget.AppCompatImageView(
    context, attrs, defStyleAttr
) {

    companion object {
//        private const val DEFAULT_BORDER_WIDTH = 0
        private const val DEFAULT_BORDER_COLOR = Color.RED

        val bgColors = arrayOf(
            Color.parseColor("#7BC862"),
            Color.parseColor("#E17076"),
            Color.parseColor("#FAA774"),
            Color.parseColor("#6EC9CB"),
            Color.parseColor("#65AADD"),
            Color.parseColor("#A695E7"),
            Color.parseColor("#EE7AAE"),
            Color.parseColor("#2196F3")
        )
    }

//    @Px
//    var borderWidth: Float = context.dpToPx(DEFAULT_BORDER_WIDTH)

//    @ColorInt
//    private var borderColor: Int = DEFAULT_BORDER_COLOR

    private var backgroundColorInt: Int = Color.TRANSPARENT
    private var initials: String = "PS"

    private val borderPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val avatarPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val initialsPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val viewRect = Rect()
    private val borderRect = Rect()

    init {
        if (attrs != null) {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.AvatarImageView)
//            borderWidth = ta.getDimension(
//                R.styleable.AvatarImageView_aiv_borderWidth,
//                context.dpToPx(DEFAULT_BORDER_WIDTH)
//            )
//            borderColor = ta.getColor(
//                R.styleable.AvatarImageView_aiv_borderColor,
//                DEFAULT_BORDER_COLOR
//            )
            initials = ta.getString(R.styleable.AvatarImageView_aiv_initials) ?: "PS"
            backgroundColorInt = ta.getColor(
                R.styleable.AvatarImageView_aiv_backgroundColor,
                Color.TRANSPARENT
            )
            ta.recycle()
        }
        scaleType = ScaleType.CENTER_CROP
        setup()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        if (w == 0)
            return
        with(viewRect) {
            left = 0
            right = w
            top = 0
            bottom = h
        }
        prepareShader(w, h)
    }

    override fun onDraw(canvas: Canvas) {

        //NOT allocate, ONLY draw
        when {
            drawable != null -> drawAvatar(canvas)
            backgroundColorInt != Color.TRANSPARENT -> drawBackground(canvas, backgroundColorInt)
            else -> drawInitials(canvas)
        }

        //resize rect
//        val half = (borderWidth / 2).toInt()
//        borderRect.set(viewRect)
//        borderRect.inset(half, half)
//        canvas.drawOval(borderRect.toRectF(), borderPaint)
    }

    override fun setImageBitmap(bm: Bitmap?) {
        super.setImageBitmap(bm)
        prepareShader(width, height)
    }

    override fun setImageDrawable(drawable: Drawable?) {
        super.setImageDrawable(drawable)
        prepareShader(width, height)
    }

    override fun setImageResource(resId: Int) {
        super.setImageResource(resId)
        prepareShader(width, height)
    }

//    override fun onSaveInstanceState(): Parcelable? {
//        val savedState = SavedState(super.onSaveInstanceState())
////        savedState.borderWidth = borderWidth
////        savedState.borderColor = borderColor
//        savedState.backgroundColor = backgroundColorInt
//
//        return savedState
//    }

//    override fun onRestoreInstanceState(state: Parcelable?) {
//        if (state is SavedState) {
//            super.onRestoreInstanceState(state)
////            borderWidth = state.borderWidth
////            borderColor = state.borderColor
//            backgroundColorInt = state.backgroundColor
//
////            with(borderPaint) {
////                color = borderColor
////                strokeWidth = borderWidth
////            }
//        } else {
//            super.onRestoreInstanceState(state)
//        }
//    }

//    fun setBorderColor(@ColorInt color: Int) {
//        borderColor = color
//        borderPaint.color = borderColor
//        invalidate()
//    }

    fun setBackgroundColorInt(@ColorInt backgroundColor: Int) {
        backgroundColorInt = backgroundColor

        initialsPaint.color = backgroundColorInt
//        drawBackground(canvas, backgroundColorInt)
        invalidate()
    }

//    fun setBorderWidth(@Dimension width: Int) {
//        borderWidth = context.dpToPx(width)
//        borderPaint.strokeWidth = borderWidth
//        invalidate()
//    }

    private fun setup() {

//        with(borderPaint) {
//            style = Paint.Style.STROKE
//            strokeWidth = borderWidth
//            color = borderColor
//        }
    }

    private fun prepareShader(w: Int, h: Int) {
        if (w == 0 || drawable == null) return
        val srcBm = drawable.toBitmap(w, h, Bitmap.Config.ARGB_8888)
        avatarPaint.shader = BitmapShader(srcBm, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
    }

    private fun drawAvatar(canvas: Canvas) {
        canvas.drawOval(viewRect.toRectF(), avatarPaint)
    }

    private fun drawBackground(canvas: Canvas, colorBackground: Int) {
        initialsPaint.color = colorBackground
        canvas.drawOval(viewRect.toRectF(), initialsPaint)
//        with(initialsPaint) {
//            color = Color.RED
//            color = borderColor
//            textAlign = Paint.Align.CENTER
//            textSize = height * 0.33f
//        }

//        val offsetY = (initialsPaint.descent() + initialsPaint.ascent()) / 2
//        canvas.drawText(
//            initials,
//            viewRect.exactCenterX(),
//            viewRect.exactCenterY() - offsetY,
//            initialsPaint
//        )
    }

    private fun drawInitials(canvas: Canvas) {
        initialsPaint.color = initialsToColor(initials)
        canvas.drawOval(viewRect.toRectF(), initialsPaint)
        with(initialsPaint) {
//            color = Color.RED
//            color = borderColor
            textAlign = Paint.Align.CENTER
            textSize = height * 0.33f
        }

        val offsetY = (initialsPaint.descent() + initialsPaint.ascent()) / 2
        canvas.drawText(
            initials,
            viewRect.exactCenterX(),
            viewRect.exactCenterY() - offsetY,
            initialsPaint
        )
    }

    private fun initialsToColor(letters: String): Int {
        val b = letters[0].toByte()
        val len = bgColors.size
        val d = b / len.toDouble()
        val index = ((d - truncate(d)) * len).toInt()
        return bgColors[index]
    }

//    private class SavedState : BaseSavedState, Parcelable {
//        var borderWidth: Float = 0f
//        var borderColor: Int = 0
//        var backgroundColor: Int = 0
//
//        constructor(superState: Parcelable?) : super(superState)
//
//        constructor(src: Parcel) : super(src) {
//            //restore state from parcel
//            borderWidth = src.readFloat()
//            borderColor = src.readInt()
//            backgroundColor = src.readInt()
//        }
//
//        override fun writeToParcel(dst: Parcel, flags: Int) {
//            //write ro parcel
//
//            super.writeToParcel(dst, flags)
//            dst.writeFloat(borderWidth)
//            dst.writeInt(borderColor)
//            dst.writeInt(backgroundColor)
//        }
//
//        override fun describeContents() = 0
//
//        companion object CREATOR : Parcelable.Creator<SavedState> {
//            override fun createFromParcel(source: Parcel) = SavedState(source)
//
//            override fun newArray(size: Int): Array<SavedState?> = arrayOfNulls(size)
//        }
//    }
}