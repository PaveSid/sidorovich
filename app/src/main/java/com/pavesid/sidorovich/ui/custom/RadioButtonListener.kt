package com.pavesid.sidorovich.ui.custom

import android.view.View

interface RadioButtonListener {
    fun onClick(view: View?)
}