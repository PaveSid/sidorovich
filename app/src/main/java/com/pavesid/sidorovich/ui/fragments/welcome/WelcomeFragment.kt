package com.pavesid.sidorovich.ui.fragments.welcome

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.databinding.FragmentWelcomeBinding

class WelcomeFragment : Fragment() {
    private var _binding: FragmentWelcomeBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding
        get() = _binding!!

    companion object {
        @JvmStatic
        fun newInstance() =
            WelcomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentWelcomeBinding.inflate(inflater, container, false)
        val view = binding.root

        initViews()

        return view
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun initViews() {
//        binding.button.visibility = View.GONE
//        apply {
//            setOnClickListener {
//                fragmentManager?.beginTransaction()
//                    ?.replace(
//                        R.id.fragment_container,
//                        DescriptionFragment.newInstance()
//                    )
//                    ?.addToBackStack("WelcomeFragment")
//                    ?.commit()
//                    ?: throw NullPointerException("Fragment Manager or smth else is null in WelcomeFragment")
//            }
//
//            text = getString(R.string.next)
//        }

        binding.name.text = getString(R.string.app_name)

        binding.welcome.text = getString(R.string.welcome)
    }
}
