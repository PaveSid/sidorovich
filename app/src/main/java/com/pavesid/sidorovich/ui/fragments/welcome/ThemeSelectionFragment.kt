package com.pavesid.sidorovich.ui.fragments.welcome

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.pavesid.sidorovich.LauncherApp.Companion.applicationContext
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.databinding.FragmentThemeSelectionBinding
import com.pavesid.sidorovich.ui.activities.MainActivity
import com.pavesid.sidorovich.viewmodels.BaseViewModel
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.THEME

/**
 * A simple [Fragment] subclass.
 */
class ThemeSelectionFragment : Fragment() {

    private var _binding: FragmentThemeSelectionBinding? = null
    private val viewModel: BaseViewModel by activityViewModels()

    // This property is only valid between onCreateView and onDestroyView.
    private val binding
        get() = _binding!!

    private val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext())

    companion object {
        @JvmStatic
        fun newInstance() =
            ThemeSelectionFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentThemeSelectionBinding.inflate(inflater, container, false)
        val view = binding.root

        initViews()

        return view
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun initViews() {

        binding.lightMode.text = getString(R.string.light_mode)
        binding.darkMode.text = getString(R.string.dark_mode)

        if (viewModel.getTheme().value == AppCompatDelegate.MODE_NIGHT_YES) {
            binding.darkMode.isChecked = true
        } else {
            binding.lightMode.isChecked = true
        }

        binding.modeGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.lightMode -> {
                    prefs.edit().apply {
                        putBoolean(THEME, false)
                        apply()
                    }
                    viewModel.switchTheme(false)
                }
                R.id.darkMode -> {
                    prefs.edit().apply {
                        putBoolean(THEME, true)
                        apply()
                    }
                    viewModel.switchTheme(true)
                }
            }
        }

        binding.button.apply {
            setOnClickListener {
                startActivity(Intent(applicationContext(), MainActivity::class.java))
                activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                prefs.edit().apply {
                    putBoolean(BaseViewModel.START_WELCOME, false)
                    apply()
                }
                activity?.finishAffinity()
            }

            text = getString(R.string.next)
        }
    }
}
