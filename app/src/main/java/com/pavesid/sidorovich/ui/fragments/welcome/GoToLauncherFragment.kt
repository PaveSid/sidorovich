package com.pavesid.sidorovich.ui.fragments.welcome

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.pavesid.sidorovich.R

/**
 * A simple [Fragment] subclass.
 */
class GoToLauncherFragment : Fragment() {

    companion object {
        @JvmStatic
        fun newInstance() =
            GoToLauncherFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_go_to_launcher, container, false)
    }
}
