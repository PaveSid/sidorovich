package com.pavesid.sidorovich.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.R
import kotlin.math.abs

class ConstraintLayoutCustom @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = R.attr.navigationViewStyle
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var x1: Float = 0f

    companion object {
        private const val SWIPE_SIZE = 150
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        /*
         * This method JUST determines whether we want to intercept the motion.
         * If we return true, onTouchEvent will be called and we do the actual
         * scrolling there.
         */
        return when (ev.actionMasked) {

            MotionEvent.ACTION_DOWN -> {
                x1 = ev.x
                false
            }

            MotionEvent.ACTION_MOVE -> {
                val xDiff: Int = abs(ev.x - x1).toInt()

                // Touch slop should be calculated using ViewConfiguration
                // constants.
                xDiff > SWIPE_SIZE
            }

            else -> {
                // In general, we don't want to intercept touch events. They should be
                // handled by the child view.
                false
            }
        }
    }
}