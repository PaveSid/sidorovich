package com.pavesid.sidorovich.ui.fragments.launcher

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.pavesid.sidorovich.LauncherApp
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.adapter.FavoritesAdapter
import com.pavesid.sidorovich.adapter.ItemAdapter
import com.pavesid.sidorovich.databinding.FragmentGridBinding
import com.pavesid.sidorovich.extensions.dpToPx
import com.pavesid.sidorovich.service.ImageLoaderService
import com.pavesid.sidorovich.service.ImageLoaderService.Companion.IMAGE_NAME_ALL
import com.pavesid.sidorovich.service.ImageLoaderService.Companion.IMAGE_NAME_GRID
import com.pavesid.sidorovich.ui.activities.MainActivity
import com.pavesid.sidorovich.ui.decoration.SpacingBottomItemDecoration
import com.pavesid.sidorovich.ui.decoration.SpacingItemDecoration
import com.pavesid.sidorovich.utils.ImageSaver
import com.pavesid.sidorovich.utils.Utils
import com.pavesid.sidorovich.viewmodels.AppViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlin.math.min

class GridFragment : BaseGridFragment() {

    private lateinit var myAdapter: ItemAdapter

    private lateinit var bottomAdapter: FavoritesAdapter

//    private lateinit var activityApp: MainActivity

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    private var mUpdateImageBroadcastReceiver: BroadcastReceiver? = null

    //    private var count = 0
    private var _binding: FragmentGridBinding? = null
    private val binding
        get() = _binding!!
    private val viewModel: AppViewModel by activityViewModels()
    private var viewWidth: Int = 0

    private var job: Job? = null

    private var canHide = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentGridBinding.inflate(inflater, container, false)

        activityApp = activity as MainActivity

        if (viewModel.getServicePeriod().value ?: 1L != 1L) {
            if (viewModel.isDifferentPictures().value == true) {
                setBackground(IMAGE_NAME_GRID)
            } else {
                setBackground(IMAGE_NAME_ALL)
            }
        } else {
            binding.gridLayout.background = null
        }

        mUpdateImageBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(
                context: Context,
                intent: Intent
            ) {
                val action = intent.action
                if (ImageLoaderService.BROADCAST_ACTION_UPDATE_IMAGE == action) {
                    when (intent.getStringExtra(ImageLoaderService.BROADCAST_PARAM_IMAGE)) {
                        IMAGE_NAME_GRID -> setBackground(IMAGE_NAME_GRID)
                        IMAGE_NAME_ALL -> setBackground(IMAGE_NAME_ALL)
                    }
                }
            }
        }

        activity?.registerReceiver(
            mUpdateImageBroadcastReceiver,
            IntentFilter(ImageLoaderService.BROADCAST_ACTION_UPDATE_IMAGE)
        )

        Utils.metricaOpenFragment(Utils.GRID)

        initializeDrawer()

//        myAdapter.updateData(viewModel.getApps().value!!)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
    }

    private fun setBackground(fileName: String) {
        var bitmap = LauncherApp.cache?.getImageFromCache(fileName)
        if (bitmap == null) {
            bitmap =
                ImageSaver.instance?.loadImage(LauncherApp.applicationContext(), fileName)
            if (bitmap == null) {
                Toast.makeText(this.context, "Image not yet", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this.context, "Image from disk", Toast.LENGTH_SHORT).show()
                LauncherApp.cache?.addImageToCache(fileName, bitmap)
            }
        } else {
            Toast.makeText(this.context, "Image from cache", Toast.LENGTH_SHORT).show()
        }
        binding.gridLayout.background = BitmapDrawable(resources, bitmap)
    }

    @ObsoleteCoroutinesApi
    override fun onStart() {
        super.onStart()
        if (viewModel.isShowPopulars().value != false) {
            job = viewModel.setPopulars(viewModel.getPeriod().value ?: 0L)
        }
    }

    override fun onDestroyView() {
        _binding = null

        if (mUpdateImageBroadcastReceiver != null) {
            activity?.unregisterReceiver(mUpdateImageBroadcastReceiver)
            mUpdateImageBroadcastReceiver = null
        }
        super.onDestroyView()
    }

    private fun initializeDrawer() {

        bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet)

        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                val margin =
                    bottomSheetBehavior.peekHeight + (bottomSheet.height - bottomSheetBehavior.peekHeight) * slideOffset
                binding.recyclerGrid.updateLayoutParams<CoordinatorLayout.LayoutParams> {
                    bottomMargin = margin.toInt()
                }
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (!canHide) {
                    binding.recyclerGrid.updateLayoutParams<CoordinatorLayout.LayoutParams> {
                        bottomMargin =
                            bottomSheetBehavior.peekHeight + viewWidth + 8.dpToPx(resources)
                    }
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
            }
        })
    }

    private fun setupRecycler() {
        setupCount()

        val width = getWidth()

        myAdapter = ItemAdapter(
            viewModel,
            activity as? MainActivity,
            1,
            (width - dpToPx * (count + 1)) / count
        )

        initViewModel()

        val manager = GridLayoutManager(this@GridFragment.context, count)
        manager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position == 0 || position == viewModel.popupSize + 1) {
                    return count
                } else {
                    1
                }
            }
        }

        binding.recyclerGrid.apply {
            adapter = myAdapter
            this.layoutManager = manager
            addItemDecoration(SpacingItemDecoration(dpToPx))
        }

        val size = min(((width - dpToPx * 5) / 4), (80.dpToPx(resources)))

        bottomAdapter = FavoritesAdapter(viewModel, activityApp, size)

        val space = (width - (size + dpToPx) * 4 - dpToPx) / 4

        binding.recyclerBottom.apply {
            adapter = bottomAdapter
            layoutManager = LinearLayoutManager(activityApp, LinearLayoutManager.HORIZONTAL, false)
            addItemDecoration(SpacingBottomItemDecoration(space))
        }
    }

    override fun onPause() {
        super.onPause()

        activityApp.actionMode?.finish()

        activityApp.favorites = -1
    }

    private fun setupCount() {
        when {
            viewModel.isCustom().value == true -> {
                count = if (viewModel.getLayout().value != true) {
                    resources.getInteger(R.integer.standard)
                } else {
                    resources.getInteger(R.integer.tight)
                }
            }
            resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT -> {
                val temp = viewModel.getPortraitCount().value ?: MainActivity.DEFAULT_NUMBER.toInt()
                count = if (temp != 0) {
                    temp
                } else {
                    MainActivity.DEFAULT_NUMBER.toInt()
                }
            }
            else -> {
                val temp =
                    viewModel.getLandscapeCount().value ?: MainActivity.DEFAULT_NUMBER.toInt()
                count = if (temp != 0) {
                    temp
                } else {
                    MainActivity.DEFAULT_NUMBER.toInt()
                }
            }
        }
    }

    private fun initViewModel() {
        viewModel.getApps()
            .observe(viewLifecycleOwner, Observer { myAdapter.updateData(it) })

        viewModel.getFavorites().observe(
            viewLifecycleOwner, Observer { bottomAdapter.updateData(it.toList()) }
        )

//        viewModel.db.contactDao().getFavoritesContact().observe(viewLifecycleOwner, Observer {
//            viewModel.setFavoritesContact(it)
//        })

//        viewModel.getFavorites().observe(viewLifecycleOwner, Observer {
//
//        })

        viewModel.isHideFavorites().observe(viewLifecycleOwner, Observer {
            if (it) {
                canHide = true
                binding.recyclerGrid.updateLayoutParams<CoordinatorLayout.LayoutParams> {
                    bottomMargin =
                        16.dpToPx(resources)//bottomSheetBehavior.peekHeight + viewWidth + 16.dpToPx(resources)
                }
            } else {
                canHide = false
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

                binding.recyclerGrid.updateLayoutParams<CoordinatorLayout.LayoutParams> {
                    bottomMargin =
                        120.dpToPx(resources)//bottomSheetBehavior.peekHeight + viewWidth + 16.dpToPx(resources)
                }
            }
        })

        viewModel.isShowPopulars().observe(viewLifecycleOwner, Observer {
            if (it) {
                viewModel.getPopulars().observe(viewLifecycleOwner, Observer { list ->
                    viewModel.setDataLaunch(list, viewModel.getCountPopular().value ?: 0)
                })
            } else {
                viewModel.setDataLaunch(emptyList(), 0)
            }
        })

        viewModel.isShowFavorites().observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.bottomSheet.visibility = View.VISIBLE
            } else {
                binding.bottomSheet.visibility = View.GONE
            }
        })
    }

    override fun onStop() {
        super.onStop()

        job?.cancel()

        Utils.metricaCloseFragment(Utils.GRID)
    }
}
