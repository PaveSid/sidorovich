package com.pavesid.sidorovich.ui.fragments

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}