package com.pavesid.sidorovich.ui.activities

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.BitmapFactory
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.ContactsContract
import android.provider.Settings
import android.text.format.DateUtils
import android.util.Log
import android.view.ActionMode
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.RemoteViews
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.pavesid.sidorovich.ui.fragments.IOnBackPressed
import com.pavesid.sidorovich.LauncherApp
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.databinding.ActivityMainBinding
import com.pavesid.sidorovich.model.App
import com.pavesid.sidorovich.model.AppEntity
import com.pavesid.sidorovich.utils.Utils
import com.pavesid.sidorovich.utils.Utils.metricaErrorWithCursor
import com.pavesid.sidorovich.viewmodels.AppViewModel
import com.pavesid.sidorovich.viewmodels.BaseViewModel
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.COUNT_POPULAR
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.DIFFERENT_PICTURES
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.HIDE_FAVORITES
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.IS_CUSTOM
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.LAND_COUNT
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.LAYOUT
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.MODE
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.PERIOD
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.PERIOD_SERVICE
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.PORT_COUNT
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.SERVICE
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.SHOW_FAVORITES
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.SHOW_POPULARS
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.THEME
import com.yandex.metrica.push.YandexMetricaPush
import kotlinx.android.synthetic.main.nav_header_nav_drawer.view.nav_image
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.lang.Exception
import java.net.URL
import java.util.concurrent.atomic.AtomicBoolean

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    private lateinit var navHostFragment: NavHostFragment

    private lateinit var navController: NavController

    private lateinit var navView: NavigationView

    private lateinit var binding: ActivityMainBinding

    lateinit var prefs: SharedPreferences

    private var oldX = Float.MIN_VALUE

    private var receiverAdd: BroadcastReceiver? = null
    private var receiverRemove: BroadcastReceiver? = null

    private var mode: Int? = 0

    private var navigate = true

    var viewTag: Int = -1

    var favorites: Int = -1

    var peek: Boolean = false

    companion object {
        const val DEFAULT_NUMBER = "4"
        const val CALL_PHONE_PERMISSION_CODE = 142
        const val READ_CONTACTS_PERMISSION_CODE = 147
        const val READ_CONTACTS_PERMISSION_DESKTOP_CODE = 148
        const val PICK_CONTACT_REQUEST = 1147
        const val PICK_CONTACT_DESKTOP_REQUEST = 1148

        const val CHANNEL_ID_1 = "1"
        const val CHANNEL_ID_2 = "2"
        const val CHANNEL_ID_3 = "3"
    }

    private val viewModel: AppViewModel by viewModels()

    var actionMode: ActionMode? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        setContentView(binding.root)

        setupViewModel(viewModel)

        if (savedInstanceState == null) {
            initData()
        }

        viewModel.getServicePeriod()

        initNavigation()

        initReceiver()

        intent.getStringExtra(YandexMetricaPush.EXTRA_PAYLOAD)?.let { _ ->
            navController.navigate(R.id.nav_image)
        }

        navView.menu.findItem(R.id.nav_button_1).actionView.findViewById<Button>(R.id.push)
            .setOnClickListener {
                sendSimplePush()
            }

        navView.menu.findItem(R.id.nav_button_2).actionView.findViewById<Button>(R.id.push)
            .setOnClickListener {
                sendSecondSimplePush("Привет")
            }
    }

    private fun sendSimplePush() {
        createNotificationChannel(CHANNEL_ID_1)
        val resultIntent = Intent(
            this,
            WelcomePageActivity::class.java
        )
        val resultPendingIntent: PendingIntent = PendingIntent.getActivity(
            this, 0, resultIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val builder: NotificationCompat.Builder = NotificationCompat.Builder(this, CHANNEL_ID_1)
            .setSmallIcon(R.drawable.ic_launch)
            .setContentTitle("Hi")
            .setContentText("Notification from UI")
            .setContentIntent(resultPendingIntent)
            .setAutoCancel(true)

        val notification: Notification = builder.build()

        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(1, notification)
    }

    private fun sendSecondSimplePush(title: String) {
        createNotificationChannel(CHANNEL_ID_2)
        val resultIntent = Intent(
            this,
            WelcomePageActivity::class.java
        )
        val resultPendingIntent: PendingIntent = PendingIntent.getActivity(
            this, 0, resultIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
// Get the layouts to use in the custom notification
        val notificationLayout = RemoteViews(packageName, R.layout.background)
        notificationLayout.setTextViewText(
            R.id.timestamp,
            DateUtils.formatDateTime(this, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME)
        )
        notificationLayout.setTextViewText(R.id.content_title, title)
        notificationLayout.setTextViewText(R.id.content_text, "Еще раз привет)")
        notificationLayout.setImageViewResource(R.id.big_icon, R.drawable.ic_launch)

        val builder: NotificationCompat.Builder =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                NotificationCompat.Builder(this, "2")
                    .setSmallIcon(R.drawable.ic_list)
                    .setContentTitle("Hi")
                    .setContentText("Notification from UI")
                    .setStyle(NotificationCompat.DecoratedCustomViewStyle())
                    .setContent(notificationLayout)
                    .setContentIntent(resultPendingIntent)
                    .setAutoCancel(true)
                    .setColorized(true)
                    .setColor(resources.getColor(R.color.colorAccent, theme))
            } else {
                NotificationCompat.Builder(this, CHANNEL_ID_2)
                    .setSmallIcon(R.drawable.ic_list)
                    .setContentTitle("Hi")
                    .setContentText("Notification from UI")
                    .setStyle(NotificationCompat.DecoratedCustomViewStyle())
                    .setContent(notificationLayout)
                    .setContentIntent(resultPendingIntent)
                    .setAutoCancel(true)
                    .setColorized(true)
                    .setColor(resources.getColor(R.color.colorAccent))
            }

        val notification: Notification = builder.build()

        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(2, notification)
    }

    private fun createNotificationChannel(CHANNEL_ID: String) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name) + CHANNEL_ID
            val descriptionText = getString(R.string.channel_description) + CHANNEL_ID
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            (navHostFragment.childFragmentManager.fragments[0] as? IOnBackPressed)?.onBackPressed()
        }
        super.onBackPressed()
    }

    override fun onDestroy() {
        if (receiverAdd != null) {
            unregisterReceiver(receiverAdd)
            receiverAdd = null
        }
        if (receiverRemove != null) {
            unregisterReceiver(receiverRemove)
            receiverRemove = null
        }
        super.onDestroy()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun initData() {
        val atomic = AtomicBoolean(true)
        CoroutineScope(Dispatchers.Main).launch {
            if (atomic.get()) {
                val list = getInstalledAppList(packageManager)
                if (atomic.get()) {
                    atomic.set(false)
                    viewModel.setApps(list)
                    viewModel.setNewMode(mode)
                }
            }
        }
    }

    private fun initReceiver() {
        val filterAdd = IntentFilter().apply {
            addAction(Intent.ACTION_INSTALL_PACKAGE)
            addAction(Intent.ACTION_PACKAGE_ADDED)
            addDataScheme("package")
        }

        val filterRemove = IntentFilter().apply {
            addAction(Intent.ACTION_PACKAGE_REMOVED)
            addAction(Intent.ACTION_UNINSTALL_PACKAGE)
            addDataScheme("package")
        }

        receiverAdd = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                Log.i("M_MainActivity", "${intent?.data?.schemeSpecificPart}")
                val info = packageManager.getPackageInfo(intent?.data?.schemeSpecificPart ?: "", 0)
                val app = App(
                    packageName = info.packageName,
                    name = packageManager.getApplicationLabel(info.applicationInfo).toString(),
                    image = packageManager.getApplicationIcon(info.applicationInfo),
                    time = info.firstInstallTime,
                    entity = AppEntity(
                        packageName = info.packageName,
                        name = packageManager.getApplicationLabel(info.applicationInfo)
                            .toString(),
                        count = 0,
                        isFavorite = -1
                    )
                )
                viewModel.addApp(app)
            }
        }

        receiverRemove = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                viewModel.updateData(intent?.data?.schemeSpecificPart)
                viewModel.deletePopular(intent?.data?.schemeSpecificPart)
            }
        }

        registerReceiver(receiverAdd, filterAdd)

        registerReceiver(receiverRemove, filterRemove)
    }

    private fun initNavigation() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        navController = navHostFragment.navController

        navView.getHeaderView(0).nav_image.apply {
            setOnClickListener {
                navController.navigate(R.id.nav_image)
                drawerLayout.closeDrawers()
            }
        }
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.nav_desktop, R.id.nav_grid, R.id.nav_list), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    fun peekApp() {
        peek = true
        navController.navigate(R.id.nav_list)
    }

    fun peekAppClose() {
        peek = false
        navController.navigate(R.id.nav_desktop)
    }

    private fun setupViewModel(viewModel: BaseViewModel) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        viewModel.switchTheme(prefs.getBoolean(THEME, false))
        viewModel.switchLayout(prefs.getBoolean(LAYOUT, false))
        viewModel.isCustom(prefs.getBoolean(IS_CUSTOM, false))
        viewModel.isShowFavorites(prefs.getBoolean(SHOW_FAVORITES, true))
        viewModel.isHideFavorites(prefs.getBoolean(HIDE_FAVORITES, false))
        viewModel.setLandscapeCount(prefs.getString(LAND_COUNT, DEFAULT_NUMBER)?.toInt())
        viewModel.setPortraitCount(prefs.getString(PORT_COUNT, DEFAULT_NUMBER)?.toInt())
        viewModel.getTheme().observe(this, Observer { updateTheme(it) })
        viewModel.isShowPopulars(prefs.getBoolean(SHOW_POPULARS, true))
        viewModel.setCountPopular(prefs.getInt(COUNT_POPULAR, 4))
        viewModel.setPeriod(prefs.getString(PERIOD, "60000")?.toLong() ?: 60000)
        viewModel.setServicePeriod(prefs.getString(PERIOD_SERVICE, "1")?.toLong() ?: 1L)
        viewModel.isDifferentPictures(prefs.getBoolean(DIFFERENT_PICTURES, false))
        val point = getPoint()
        viewModel.setService(prefs.getString(SERVICE, "1")?.toInt() ?: 1, point.x, point.y)
        mode = prefs.getString(MODE, "0")?.toInt()
    }

    private fun updateTheme(mode: Int) {
        delegate.localNightMode = mode
    }

    private suspend fun getInstalledAppList(
        packageManager: PackageManager
    ): List<App> {
        withContext(Dispatchers.Main) {
            binding.progressBar.visibility = View.VISIBLE
            binding.tvProgressCircle.visibility = View.VISIBLE
        }

        val list = mutableListOf<App>()
        withContext(Dispatchers.IO) {

            val intent = Intent(Intent.ACTION_MAIN, null).apply {
                addCategory(Intent.CATEGORY_LAUNCHER)
            }

            val untreatedAppList = packageManager.queryIntentActivities(intent, 0)

            val size = untreatedAppList.size
            for ((count, resolveInfo) in untreatedAppList.withIndex()) {

                val app = App(
                    packageName = resolveInfo.activityInfo.packageName,
                    name = resolveInfo.activityInfo.loadLabel(packageManager).toString(),
                    image = packageManager.getApplicationIcon(resolveInfo.activityInfo.applicationInfo),
                    time = packageManager.getPackageInfo(
                        resolveInfo.activityInfo.packageName,
                        0
                    ).firstInstallTime,
                    entity = AppEntity(
                        packageName = resolveInfo.activityInfo.packageName,
                        name = resolveInfo.activityInfo.loadLabel(packageManager).toString(),
                        count = 0,
                        isFavorite = -1
                    )
                )

                if (!list.contains(app) && "com.pavesid.sidorovich" != app.packageName) {
                    list.add(app)
                }
                setProgressInUI(((count.toFloat() / size) * 100).toInt())
            }
        }
        withContext(Dispatchers.Main) {
            binding.progressBar.visibility = View.GONE
            binding.tvProgressCircle.visibility = View.GONE
        }
        return list
    }

    private suspend fun setProgressInUI(count: Int) {
        withContext(Dispatchers.Main) {
            binding.progressBar.progress = count
            binding.tvProgressCircle.text = "$count %"
        }
    }

    fun deleteApp(packageName: String?) {
        startActivity(
            Intent(Intent.ACTION_DELETE, "package:$packageName".toUri())
        )
    }

    fun openSettings(packageName: String?) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.data = "package:$packageName".toUri()
        startActivity(intent)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.actionMasked) {
            MotionEvent.ACTION_UP -> {
                oldX = Float.MIN_VALUE
                navigate = true
            }
            MotionEvent.ACTION_MOVE -> {
                if (navigate) {
                    if (oldX == Float.MIN_VALUE) {
                        oldX = event.x
                    } else {
                        if (event.x < oldX) {
                            navigate(navController.currentDestination?.label.toString(), true)
                        } else {
                            navigate(navController.currentDestination?.label.toString(), false)
                        }
                        navigate = false
                    }
                }
            }
        }
        return super.onTouchEvent(event)
    }

    private fun navigate(label: String?, isRight: Boolean) {
        if (isRight) {
            when (label) {
                getString(R.string.grid) -> navController.navigate(R.id.grid_to_list)
                getString(R.string.list) -> navController.navigate(R.id.list_to_settings)
                getString(R.string.settings) -> navController.navigate(R.id.settings_to_image)
                getString(R.string.desktop) -> navController.navigate(R.id.desktop_to_grid)
                else -> {
                    val toolbar: Toolbar = this.findViewById(R.id.toolbar)
                    this.setSupportActionBar(toolbar)
                    this.supportActionBar?.show()
                    navController.navigate(R.id.image_to_desktop)
                }
            }
        } else {
            when (label) {
                getString(R.string.grid) -> navController.navigate(R.id.grid_to_desktop)
                getString(R.string.list) -> navController.navigate(R.id.list_to_grid)
                getString(R.string.settings) -> navController.navigate(R.id.settings_to_list)
                getString(R.string.desktop) -> navController.navigate(R.id.desktop_to_image)
                else -> {
                    val toolbar: Toolbar = this.findViewById(R.id.toolbar)
                    this.setSupportActionBar(toolbar)
                    this.supportActionBar?.show()
                    navController.navigate(R.id.image_to_settings)
                }
            }
        }
    }

    // Function to check and request permission
    fun checkPermission(permission: String, requestCode: Int, phone: String) {
        when (requestCode) {
            CALL_PHONE_PERMISSION_CODE -> {
                // Checking if permission is not granted
                if (ContextCompat.checkSelfPermission(
                        this,
                        permission
                    ) == PackageManager.PERMISSION_DENIED
                ) {
                    ActivityCompat
                        .requestPermissions(
                            this, arrayOf(permission),
                            requestCode
                        )
                } else {
                    dialPhoneNumber(phone)
                }
            }
            READ_CONTACTS_PERMISSION_CODE -> {
                // Checking if permission is not granted
                if (ContextCompat.checkSelfPermission(
                        this,
                        permission
                    ) == PackageManager.PERMISSION_DENIED
                ) {
                    ActivityCompat
                        .requestPermissions(
                            this, arrayOf(permission),
                            requestCode
                        )
                } else {
                    Intent(Intent.ACTION_PICK).also { intent ->
                        intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                        startActivityForResult(intent, PICK_CONTACT_REQUEST)
                    }
                }
            }
            READ_CONTACTS_PERMISSION_DESKTOP_CODE -> {
                // Checking if permission is not granted
                if (ContextCompat.checkSelfPermission(
                        this,
                        permission
                    ) == PackageManager.PERMISSION_DENIED
                ) {
                    ActivityCompat
                        .requestPermissions(
                            this, arrayOf(permission),
                            requestCode
                        )
                } else {
                    Intent(Intent.ACTION_PICK).also { intent ->
                        intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                        startActivityForResult(intent, PICK_CONTACT_DESKTOP_REQUEST)
                    }
                }
            }
        }
    }

    private fun dialPhoneNumber(phoneNumber: String) {
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:$phoneNumber")
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty()
            && grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            if (requestCode == READ_CONTACTS_PERMISSION_CODE) {
                Intent(Intent.ACTION_PICK).also { intent ->
                    intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                    startActivityForResult(intent, PICK_CONTACT_REQUEST)
                }
            }
            if (requestCode == READ_CONTACTS_PERMISSION_DESKTOP_CODE) {
                Intent(Intent.ACTION_PICK).also { intent ->
                    intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                    startActivityForResult(intent, PICK_CONTACT_DESKTOP_REQUEST)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var cursor: Cursor? = null
        if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {
            if (data != null) {
                try {
                    val contactUri = data.data
                    val projection = arrayOf(
                        ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                    )
                    cursor = this.contentResolver?.query(
                        contactUri!!, projection, null, null, null
                    )
                    cursor!!.moveToFirst()
                    val column =
                        cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                    val columnName =
                        cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                    val number = cursor.getString(column)
                    val name = cursor.getString(columnName)

                    viewModel.addContactToFavorites(number, name, viewTag)
                } catch (e: Exception) {
                    metricaErrorWithCursor(e)
                } finally {
                    cursor?.close()
                }
            }
        } else if (requestCode == PICK_CONTACT_DESKTOP_REQUEST && resultCode == RESULT_OK) {
            if (data != null) {
                try {
                    val contactUri = data.data
                    val projection = arrayOf(
                        ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                    )
                    cursor = this.contentResolver?.query(
                        contactUri!!, projection, null, null, null
                    )
                    cursor!!.moveToFirst()
                    val column =
                        cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                    val columnName =
                        cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                    val number = cursor.getString(column)
                    val name = cursor.getString(columnName)

                    viewModel.addContactToDesktop(number, name, viewModel.desktopSize + 1)
                } catch (e: Exception) {
                    metricaErrorWithCursor(e)
                } finally {
                    cursor?.close()
                }
            }
        }
    }

    private fun getPoint(): Point {
        val display = windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        return size
    }

    fun openWebPage(url: String) {
        val webPage: Uri = if (url.startsWith("https://") || url.startsWith("http://")) {
            Uri.parse(url)
        } else {
            Uri.parse("https://$url")
        }

        val intent = Intent(Intent.ACTION_VIEW, webPage)
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    fun loadDrawable(urlStr: String): Drawable? {
        try {
            val webPage: String =
                if (urlStr.startsWith("https://") || urlStr.startsWith("http://")) {
                    urlStr
                } else {
                    "https://$urlStr"
                }
            val urlCheck = URL(webPage)
            val urlConnectionCheck = urlCheck.openConnection()
            val inputStreamCheck = urlConnectionCheck.getInputStream()
            inputStreamCheck.close()
            val url = URL("https://favicon.yandex.net/favicon/$urlStr?size=120")
            val urlConnection = url.openConnection()
            urlConnection.useCaches = true
            val inputStream = urlConnection.getInputStream()
            val buffer = ByteArrayOutputStream()
            var nRead: Int
            val data = ByteArray(16384)
            while (inputStream.read(data, 0, data.size).also { nRead = it } != -1) {
                buffer.write(data, 0, nRead)
            }
            buffer.flush()
            inputStream.close()
            val bitmap = buffer.toByteArray()
            val drawable = BitmapFactory.decodeByteArray(bitmap, 0, bitmap.size)
            LauncherApp.cache?.addImageToCache(urlStr, drawable)
            return BitmapDrawable(resources, drawable)
        } catch (e: IOException) {
            Utils.metricaErrorNetwork(e)
        }
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            resources?.getDrawable(
                R.drawable.cat,
                theme
            )
        } else {
            resources?.getDrawable(R.drawable.cat)
        }
    }

//    fun setBackground(fileName: String) {
//        var bitmap = LauncherApp.cache?.getImageFromCache(fileName)
//        if (bitmap == null) {
//            bitmap =
//                ImageSaver.instance?.loadImage(applicationContext, fileName)
//            if (bitmap == null) {
//                Toast.makeText(this, "Image not yet", Toast.LENGTH_SHORT).show()
//            } else {
//                Toast.makeText(this, "Image from disk", Toast.LENGTH_SHORT).show()
//                LauncherApp.cache?.addImageToCache(fileName, bitmap)
//            }
//        } else {
//            Toast.makeText(this, "Image from cache", Toast.LENGTH_SHORT).show()
//        }
//        when (fileName) {
//            IMAGE_NAME_ALL -> {
//                binding.drawerLayout.background = BitmapDrawable(resources, bitmap)
//            }
//        }
//    }

    fun setBackgroundEmpty() {
        binding.drawerLayout.background = null
    }
}
