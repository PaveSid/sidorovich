package com.pavesid.sidorovich.ui.fragments.launcher

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pavesid.sidorovich.LauncherApp
import com.pavesid.sidorovich.adapter.ItemAdapter
import com.pavesid.sidorovich.databinding.FragmentListBinding
import com.pavesid.sidorovich.service.ImageLoaderService
import com.pavesid.sidorovich.service.ImageLoaderService.Companion.IMAGE_NAME_ALL
import com.pavesid.sidorovich.service.ImageLoaderService.Companion.IMAGE_NAME_LIST
import com.pavesid.sidorovich.ui.activities.MainActivity
import com.pavesid.sidorovich.utils.ImageSaver
import com.pavesid.sidorovich.utils.Utils
import com.pavesid.sidorovich.viewmodels.AppViewModel
import kotlinx.coroutines.Job

class ListFragment : Fragment() {

    private var _binding: FragmentListBinding? = null
    private val binding
        get() = _binding!!
    private val viewModel: AppViewModel by activityViewModels()

    private var job: Job? = null

    private lateinit var myAdapter: ItemAdapter

    private var mUpdateImageBroadcastReceiver: BroadcastReceiver? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListBinding.inflate(layoutInflater)

        Utils.metricaOpenFragment(Utils.LIST)

//        Log.d("M_list_isDiff", "${viewModel.isDifferentPictures().value == true}")
        if (viewModel.getServicePeriod().value ?: 1L != 1L) {
            if (viewModel.isDifferentPictures().value == true) {
                setBackground(IMAGE_NAME_LIST)
//                Log.d("M_list_isDiff", IMAGE_NAME_LIST)
            } else {
//                Log.d("M_list_isDiff", IMAGE_NAME_ALL)
                setBackground(IMAGE_NAME_ALL)
            }
        } else {
            binding.recycler.background = null
        }

        mUpdateImageBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(
                context: Context,
                intent: Intent
            ) {
                val action = intent.action
                if (ImageLoaderService.BROADCAST_ACTION_UPDATE_IMAGE == action) {
                    when (intent.getStringExtra(ImageLoaderService.BROADCAST_PARAM_IMAGE)) {
                        IMAGE_NAME_LIST -> setBackground(IMAGE_NAME_LIST)
                        IMAGE_NAME_ALL -> setBackground(IMAGE_NAME_ALL)
                    }
                }
            }
        }

        activity?.registerReceiver(
            mUpdateImageBroadcastReceiver,
            IntentFilter(ImageLoaderService.BROADCAST_ACTION_UPDATE_IMAGE)
        )

        return binding.root
    }

    private fun setBackground(fileName: String) {
        var bitmap = LauncherApp.cache?.getImageFromCache(fileName)
        if (bitmap == null) {
            bitmap =
                ImageSaver.instance?.loadImage(LauncherApp.applicationContext(), fileName)
            if (bitmap == null) {
                Toast.makeText(this.context, "Image not yet", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this.context, "Image from disk", Toast.LENGTH_SHORT).show()
                LauncherApp.cache?.addImageToCache(fileName, bitmap)
            }
        } else {
            Toast.makeText(this.context, "Image from cache", Toast.LENGTH_SHORT).show()
        }
        binding.recycler.background = BitmapDrawable(resources, bitmap)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
    }

    override fun onStart() {
        super.onStart()
        if (viewModel.isShowPopulars().value != false) {
            job = viewModel.setPopulars(viewModel.getPeriod().value ?: 0L)
        }
    }

    private fun initViews() {

        setupRecycler()

//        binding.fab.hide()
    }

    private fun setupRecycler() {
        myAdapter = ItemAdapter(viewModel, activity as? MainActivity)

        initViewModel()

        binding.recycler.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(this@ListFragment.context)

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    myAdapter.scrollDirection = if (dy > 0) {
                        ItemAdapter.ScrollDirection.DOWN
                    } else {
                        ItemAdapter.ScrollDirection.UP
                    }
                }
            })
        }
    }

    override fun onStop() {
        super.onStop()
        job?.cancel()

        (activity as MainActivity).apply {
            peek = false
            actionMode?.finish()
        }

        Utils.metricaCloseFragment(Utils.LIST)
    }

    override fun onDestroyView() {
        _binding = null

        if (mUpdateImageBroadcastReceiver != null) {
            activity?.unregisterReceiver(mUpdateImageBroadcastReceiver)
            mUpdateImageBroadcastReceiver = null
        }
        super.onDestroyView()
    }

    private fun initViewModel() {
        viewModel.getApps()
            .observe(viewLifecycleOwner, Observer { list -> myAdapter.updateData(list) })

        viewModel.isShowPopulars().observe(viewLifecycleOwner, Observer {
            if (it) {
                viewModel.getPopulars().observe(viewLifecycleOwner, Observer { list ->
                    viewModel.setDataLaunch(list, viewModel.getCountPopular().value ?: 0)
                })
            } else {
                viewModel.setDataLaunch(emptyList(), 0)
            }
        })
    }
}
