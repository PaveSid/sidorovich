package com.pavesid.sidorovich.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup

class CustomRadioGroup(context: Context?, attrs: AttributeSet?) : RadioGroup(context, attrs) {

    companion object {
        private var onClickListener: RadioButtonListener? = null
        fun setOnClickListener(onClickListener: RadioButtonListener?) {
            this.onClickListener = onClickListener
        }
    }

    private val group: ArrayList<RadioButton?> = ArrayList()

    override fun addView(child: View?, params: ViewGroup.LayoutParams?) {
        if (child is LinearLayout) {
            var childRadioButton: RadioButton? = null
            for (i in 0..child.childCount) {
                if (child.getChildAt(i) is RadioButton) {
                    childRadioButton = child.getChildAt(i) as? RadioButton
                    group.add(childRadioButton)
                }
            }
            child.setOnClickListener {
                setAllButtonsToUnselectedState()
                setSelectedButtonToSelectedState(childRadioButton)
                initOnClickListener(childRadioButton)
            }
        }
        super.addView(child, params)
    }

    private fun setAllButtonsToUnselectedState() {
        for (i in 0 until group.size) {
            group[i]?.isChecked = false
        }
    }

    private fun setSelectedButtonToSelectedState(selectedButton: RadioButton?) {
        selectedButton?.isChecked = true
    }

    private fun initOnClickListener(selectedButton: View?) {
        if (onClickListener != null) {
            onClickListener!!.onClick(selectedButton)
        }
    }
}