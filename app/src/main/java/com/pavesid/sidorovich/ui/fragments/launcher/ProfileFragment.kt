package com.pavesid.sidorovich.ui.fragments.launcher

import android.Manifest
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.pavesid.sidorovich.ui.fragments.IOnBackPressed
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.databinding.FragmentProfileBinding
import com.pavesid.sidorovich.model.User
import com.pavesid.sidorovich.ui.activities.MainActivity
import com.pavesid.sidorovich.viewmodels.AppViewModel
import com.pavesid.sidorovich.viewmodels.BaseViewModel

class ProfileFragment : Fragment(),
    IOnBackPressed {

    private var _binding: FragmentProfileBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: AppViewModel by activityViewModels()

    private lateinit var viewFields: Set<EditText>

    private lateinit var activityApp: MainActivity

    private var key = false

    private lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.settings_menu, menu)

        this.menu = menu

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)

        initViews()

        getUser(viewModel.getUser().value)

        initViewModel()

        activityApp = activity as MainActivity

        if (activityApp.prefs.getBoolean(BaseViewModel.HINT_PROFILE, true)) {
            binding.hintProfile.setOnClickListener {
                it.visibility = View.GONE
                activityApp.prefs.edit().apply {
                    putBoolean(BaseViewModel.HINT_PROFILE, false)
                    apply()
                }
            }
        } else {
            binding.hintProfile.visibility = View.GONE
        }

        return binding.root
    }

    private fun initViews() {

        viewFields = setOf(
            binding.etCallMobile,
            binding.etCallWork,
            binding.etMailPersonal,
            binding.etMailWork,
            binding.etAddressHome,
            binding.etAddressWork,
            binding.etSocial
        )
        binding.etMailPersonal.setOnClickListener {
            if (it is EditText) {
                composeEmail(arrayOf(it.text.toString()), "Hello from Sidorovich app!!!")
            }
        }

        binding.etMailWork.setOnClickListener {
            if (it is EditText) {
                composeEmail(arrayOf(it.text.toString()), "Hello from Sidorovich app!!!")
            }
        }

        binding.etCallMobile.setOnClickListener {
            if (it is EditText) {
                activityApp.checkPermission(
                    Manifest.permission.CALL_PHONE,
                    MainActivity.CALL_PHONE_PERMISSION_CODE,
                    it.text.toString()
                )
            }
        }

        binding.etCallWork.setOnClickListener {
            if (it is EditText) {
                activityApp.checkPermission(
                    Manifest.permission.CALL_PHONE,
                    MainActivity.CALL_PHONE_PERMISSION_CODE,
                    it.text.toString()
                )
            }
        }

        binding.etSocial.setOnClickListener {
            if (it is EditText) {
                activityApp.openWebPage(it.text.toString())
            }
        }

        binding.etAddressHome.setOnClickListener {
            if (it is EditText) {
                goToMap(it.text.toString())
            }
        }

        binding.etAddressWork.setOnClickListener {
            if (it is EditText) {
                goToMap(it.text.toString())
            }
        }
    }

    private fun saveUser() {
        val user = User()
        user.mobile = binding.etCallMobile.text.toString()
        user.workPhone = binding.etCallWork.text.toString()
        user.personalMail = binding.etMailPersonal.text.toString()
        user.workMail = binding.etMailWork.text.toString()
        user.personalAddress = binding.etAddressHome.text.toString()
        user.workAddress = binding.etAddressWork.text.toString()
        user.gitlab = binding.etSocial.text.toString()
        viewModel.updateUser(user)
    }

    private fun getUser(user: User?) {
        if (user != null) {
            binding.etCallMobile.setText(user.mobile)
            binding.etCallWork.setText(user.workPhone)
            binding.etMailPersonal.setText(user.personalMail)
            binding.etMailWork.setText(user.workMail)
            binding.etAddressHome.setText(user.personalAddress)
            binding.etAddressWork.setText(user.workAddress)
            binding.etSocial.setText(user.gitlab)
        }
    }

    private fun showCurrentMode(isEdit: Boolean) {
        if (!isEdit) {
            val inputMethodManager = activityApp.getSystemService(Context.INPUT_METHOD_SERVICE)
            if (inputMethodManager is InputMethodManager) {
                inputMethodManager.hideSoftInputFromWindow(
                    activityApp.currentFocus?.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
//            activityApp.supportActionBar?.
            menu.getItem(0).icon = ContextCompat.getDrawable(activityApp, R.drawable.ic_edit)
            saveUser()
        } else {
            menu.getItem(0).icon = ContextCompat.getDrawable(activityApp, R.drawable.ic_save)
        }
        for (v in viewFields) {
            v.isClickable = isEdit
            v.isFocusable = isEdit
            v.isFocusableInTouchMode = isEdit
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.edit -> {
                key = !key
                showCurrentMode(key)
                true
            }
            else -> {
                activityApp.onBackPressed()
                true
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activityApp.supportActionBar?.hide()
            activityApp.setSupportActionBar(binding.toolbar)
            activityApp.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        } else {
            activityApp.supportActionBar?.title = getString(R.string.name)
        }
    }

    override fun onStop() {
        super.onStop()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val toolbar: Toolbar = activityApp.findViewById(R.id.toolbar)
            activityApp.setSupportActionBar(toolbar)
            activityApp.supportActionBar?.show()
        }
    }

    override fun onBackPressed(): Boolean {
        val toolbar: Toolbar = activityApp.findViewById(R.id.toolbar)
        activityApp.setSupportActionBar(toolbar)
        activityApp.supportActionBar?.show()
        return true
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun composeEmail(addresses: Array<String>, subject: String) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:") // only email apps should handle this
            putExtra(Intent.EXTRA_EMAIL, addresses)
            putExtra(Intent.EXTRA_SUBJECT, subject)
        }
        if (intent.resolveActivity(activityApp.packageManager) != null) {
            startActivity(intent)
        }
    }

    private fun goToMap(address: String) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("geo:0,0?q=$address")
        )
        if (intent.resolveActivity(activityApp.packageManager) != null) {
            startActivity(intent)
        }
    }

//    private fun openWebPage(url: String) {
//        val webpage: Uri = if (url.startsWith("https://") || url.startsWith("http://")) {
//            Uri.parse(url)
//        } else {
//            Uri.parse("https://$url")
//        }
//
//        val intent = Intent(Intent.ACTION_VIEW, webpage)
//        if (intent.resolveActivity(activityApp.packageManager) != null) {
//            startActivity(intent)
//        }
//    }

    private fun initViewModel() {
        viewModel.getUser().observe(viewLifecycleOwner, Observer {
            getUser(it)
        })
    }
}
