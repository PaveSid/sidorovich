package com.pavesid.sidorovich.ui.fragments.launcher

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.preference.ListPreference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SeekBarPreference
import androidx.preference.SwitchPreferenceCompat
import com.pavesid.sidorovich.service.ImageLoaderService
import com.pavesid.sidorovich.LauncherApp
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.utils.Utils
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.COUNT_POPULAR
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.HIDE_FAVORITES
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.IS_CUSTOM
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.MODE
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.LAND_COUNT
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.LAYOUT
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.PERIOD
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.PORT_COUNT
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.SHOW_FAVORITES
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.SHOW_POPULARS
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.THEME
import com.pavesid.sidorovich.viewmodels.AppViewModel
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.DIFFERENT_PICTURES
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.PERIOD_SERVICE
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.SERVICE

class PreferenceSettingsFragment : PreferenceFragmentCompat() {

    private val viewModel: AppViewModel by activityViewModels()

    companion object {
        const val PENDING_CODE = 232
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        Utils.metricaOpenFragment(Utils.SETTINGS)

        initViews()

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }

    private fun initViews() {
        val switchTheme: SwitchPreferenceCompat? = findPreference(THEME)
        val isCustomLayout: SwitchPreferenceCompat? = findPreference(IS_CUSTOM)
        val switchLayout: SwitchPreferenceCompat? = findPreference(LAYOUT)
        val custom: PreferenceCategory? = findPreference("custom")
        val portraitCount: ListPreference? = findPreference(PORT_COUNT)
        val landscapeCount: ListPreference? = findPreference(LAND_COUNT)
        val mode: ListPreference? = findPreference(MODE)
        val showFavorites: SwitchPreferenceCompat? = findPreference(SHOW_FAVORITES)
        val hideFavorites: SwitchPreferenceCompat? = findPreference(HIDE_FAVORITES)
        val showPopulars: SwitchPreferenceCompat? = findPreference(SHOW_POPULARS)
        val countPopulars: SeekBarPreference? = findPreference(COUNT_POPULAR)
        val period: ListPreference? = findPreference(PERIOD)
        val service: ListPreference? = findPreference(SERVICE)
        val servicePeriod: ListPreference? = findPreference(PERIOD_SERVICE)
        val differentPictures: SwitchPreferenceCompat? = findPreference(DIFFERENT_PICTURES)

        val isCustom = isCustomLayout?.isChecked != false

        switchLayout?.isVisible = isCustom
        custom?.isVisible = !isCustom

        switchTheme?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is Boolean) {
                viewModel.switchTheme(newValue)
            }
            return@setOnPreferenceChangeListener true
        }

        isCustomLayout?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is Boolean) {
                viewModel.isCustom(newValue)
            }
            switchLayout?.isVisible = newValue != false
            custom?.isVisible = newValue == false
            return@setOnPreferenceChangeListener true
        }

        switchLayout?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is Boolean) {
                viewModel.switchLayout(newValue)
            }
            return@setOnPreferenceChangeListener true
        }

        showFavorites?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is Boolean) {
                viewModel.isShowFavorites(newValue)
            }
            return@setOnPreferenceChangeListener true
        }

        service?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is String) {
                val point = getPoint()
                viewModel.setService(newValue.toInt(), point.x, point.y)

                val isDifferent = viewModel.isDifferentPictures().value ?: false
                when (viewModel.getServicePeriod().value) {
                    1L -> {
                        cancelAlarmMain()
                        cancelAlarmDifferent()
                    }
                    0L -> {
                        cancelAlarmMain()
                        cancelAlarmDifferent()

                        val intent = if (isDifferent) {
                            Intent(
                                activity,
                                ImageLoaderService::class.java
                            )
                                .setAction(ImageLoaderService.SERVICE_ACTION_LOAD_IMAGE_DIFFERENT)
                                .putExtra("service", viewModel.getService().value ?: "https://www.random.org/bitmaps/?format=png&width=200&height=300&zoom=1")
                        } else {
                            Intent(
                                activity,
                                ImageLoaderService::class.java
                            )
                                .setAction(ImageLoaderService.SERVICE_ACTION_LOAD_IMAGE_ALL)
                                .putExtra("service", viewModel.getService().value ?: "https://www.random.org/bitmaps/?format=png&width=200&height=300&zoom=1")
                        }

                        activity?.startService(intent)
                    }
                    else -> {
                        if (isDifferent) {
                            cancelAlarmMain()
                            createAlarmDifferent(
                                viewModel.getService().value ?: "https://www.random.org/bitmaps/?format=png&width=200&height=300&zoom=1",
                                viewModel.getServicePeriod().value ?: 1L
                            )
                        } else {
                            cancelAlarmDifferent()
                            createAlarmMain(
                                viewModel.getService().value ?: "https://www.random.org/bitmaps/?format=png&width=200&height=300&zoom=1",
                                viewModel.getServicePeriod().value ?: 1L
                            )
                        }
                    }
                }
            }
            return@setOnPreferenceChangeListener true
        }

        hideFavorites?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is Boolean) {
                viewModel.isHideFavorites(newValue)
            }
            return@setOnPreferenceChangeListener true
        }

        showPopulars?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is Boolean) {
                viewModel.isShowPopulars(newValue)
            }
            return@setOnPreferenceChangeListener true
        }

        differentPictures?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is Boolean) {
                viewModel.isDifferentPictures(newValue)
                if (newValue) {
                    cancelAlarmMain()
                    createAlarmDifferent(
                        viewModel.getService().value ?: "https://www.random.org/bitmaps/?format=png&width=200&height=300&zoom=1",
                        viewModel.getServicePeriod().value ?: 1L
                    )
                } else {
                    cancelAlarmDifferent()
                    createAlarmMain(
                        viewModel.getService().value ?: "https://www.random.org/bitmaps/?format=png&width=200&height=300&zoom=1",
                        viewModel.getServicePeriod().value ?: 1L
                    )
                }
            }
            return@setOnPreferenceChangeListener true
        }

        countPopulars?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is Int) {
                viewModel.setCountPopular(newValue)
            }
            return@setOnPreferenceChangeListener true
        }

        period?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is String) {
                viewModel.setPeriod(newValue.toLong())
            }
            return@setOnPreferenceChangeListener true
        }

        servicePeriod?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is String) {
                viewModel.setServicePeriod(newValue.toLong())
                val isDifferent = viewModel.isDifferentPictures().value ?: false
                when (viewModel.getServicePeriod().value ?: 1L) {
                    0L -> {
                        cancelAlarmMain()
                        cancelAlarmDifferent()
                        val intent = if (isDifferent) {
                            Intent(
                                activity,
                                ImageLoaderService::class.java
                            )
                                .setAction(ImageLoaderService.SERVICE_ACTION_LOAD_IMAGE_DIFFERENT)
                                .putExtra("service", viewModel.getService().value ?: "https://www.random.org/bitmaps/?format=png&width=200&height=300&zoom=1")
                        } else {
                            Intent(
                                activity,
                                ImageLoaderService::class.java
                            )
                                .setAction(ImageLoaderService.SERVICE_ACTION_LOAD_IMAGE_ALL)
                                .putExtra("service", viewModel.getService().value ?: "https://www.random.org/bitmaps/?format=png&width=200&height=300&zoom=1")
                        }

                        activity?.startService(intent)
                    }
                    1L -> {
                        cancelAlarmMain()
                        cancelAlarmDifferent()
                    }
                    60000L -> {
                        if (isDifferent) {
                            cancelAlarmMain()
                            createAlarmDifferent(
                                viewModel.getService().value ?: "https://www.random.org/bitmaps/?format=png&width=200&height=300&zoom=1",
                                viewModel.getServicePeriod().value ?: 1L
                            )
                        } else {
                            cancelAlarmDifferent()
                            createAlarmMain(
                                viewModel.getService().value ?: "https://www.random.org/bitmaps/?format=png&width=200&height=300&zoom=1",
                                viewModel.getServicePeriod().value ?: 1L
                            )
                        }
                    }
                }
            }
            return@setOnPreferenceChangeListener true
        }

        mode?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is String) {
                viewModel.setNewMode(newValue.toInt())
            }
            return@setOnPreferenceChangeListener true
        }

        portraitCount?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is String) {
                viewModel.setPortraitCount(newValue.toInt())
            }
            return@setOnPreferenceChangeListener true
        }

        landscapeCount?.setOnPreferenceChangeListener { _, newValue ->
            if (newValue is String) {
                viewModel.setLandscapeCount(newValue.toInt())
            }
            return@setOnPreferenceChangeListener true
        }
    }

    override fun onStop() {
        super.onStop()

        Utils.metricaCloseFragment(Utils.SETTINGS)
    }

    private fun createAlarmMain(service: String, period: Long) {
        if (period != 1L) {
            Log.d("fdf", "Create Alarm")
            val intent = Intent(
                activity,
                ImageLoaderService::class.java
            )
                .setAction(ImageLoaderService.SERVICE_ACTION_LOAD_IMAGE_ALL)
                .putExtra("service", service)

            val pi = PendingIntent.getService(activity, PENDING_CODE, intent,
                PendingIntent.FLAG_CANCEL_CURRENT
            )
            Log.d("Preference", "Service create")
            Log.d("Preference", service)
            LauncherApp.getAlarmManager()
                .setRepeating(
                    AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime(),
                    period,
                    pi
                )
        }
    }

    private fun createAlarmDifferent(service: String, period: Long) {
        if (period != 1L) {
            Log.d("fdf", "Create Alarm")
            val intent = Intent(
                activity,
                ImageLoaderService::class.java
            )
                .setAction(ImageLoaderService.SERVICE_ACTION_LOAD_IMAGE_DIFFERENT)
                .putExtra("service", service)

            val pi = PendingIntent.getService(activity, PENDING_CODE, intent,
                PendingIntent.FLAG_CANCEL_CURRENT
            )
            Log.d("Preference", "Service create different")
            Log.d("Preference", service)
            LauncherApp.getAlarmManager()
                .setRepeating(
                    AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime(),
                    period,
                    pi
                )
        }
    }

    private fun cancelAlarmMain() {
        val intent = Intent(
            activity,
            ImageLoaderService::class.java
        )
            .setAction(ImageLoaderService.SERVICE_ACTION_LOAD_IMAGE_ALL)
            .putExtra("service", viewModel.getService().value)
        val pendingIntent =
            PendingIntent.getService(
                activity, PENDING_CODE, intent, PendingIntent.FLAG_NO_CREATE
            )
        if (pendingIntent != null) {
            LauncherApp.getAlarmManager().cancel(pendingIntent)
        }
    }

    private fun cancelAlarmDifferent() {
        val intent = Intent(
            activity,
            ImageLoaderService::class.java
        )
            .setAction(ImageLoaderService.SERVICE_ACTION_LOAD_IMAGE_DIFFERENT)
            .putExtra("service", viewModel.getService().value)
        val pendingIntent =
            PendingIntent.getService(
                activity, PENDING_CODE, intent, PendingIntent.FLAG_NO_CREATE
            )
        if (pendingIntent != null) {
            LauncherApp.getAlarmManager().cancel(pendingIntent)
        }
    }

    private fun getPoint(): Point {
        val display = activity?.windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        return size
    }
}