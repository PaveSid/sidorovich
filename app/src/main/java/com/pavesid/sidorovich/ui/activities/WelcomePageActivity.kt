package com.pavesid.sidorovich.ui.activities

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.adapter.ViewPagerAdapter
import com.pavesid.sidorovich.databinding.ActivityWelcomePageBinding
import com.pavesid.sidorovich.ui.decoration.DepthPageTransformer
import com.pavesid.sidorovich.viewmodels.BaseViewModel
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.IS_CUSTOM
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.LAND_COUNT
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.LAYOUT
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.PORT_COUNT
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.THEME

class WelcomePageActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWelcomePageBinding
    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomePageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        choiceActivity()
    }

    private fun choiceActivity() {
        if (prefs.getBoolean(BaseViewModel.START_WELCOME, true)) {
            setupViewModel()
            val adapter = ViewPagerAdapter(supportFragmentManager)
            binding.viewPager.adapter = adapter
            binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {
                }

                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {

                    val lastIdx: Int = adapter.count - 2
                    if (position == lastIdx && positionOffsetPixels > 1) {
                        startActivity(Intent(applicationContext, MainActivity::class.java))
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                        prefs.edit().apply {
                            putBoolean(BaseViewModel.START_WELCOME, false)
                            apply()
                        }
                        finishAffinity()
                    }
                }

                override fun onPageSelected(position: Int) {
                }
            })
            binding.viewPager.setPageTransformer(true, DepthPageTransformer())

            binding.tabLayout.setupWithViewPager(binding.viewPager, true)
        } else {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun setupViewModel() {
        val model: BaseViewModel by viewModels()
        model.switchTheme(prefs.getBoolean(THEME, false))
        model.switchLayout(prefs.getBoolean(LAYOUT, false))
        model.isCustom(prefs.getBoolean(IS_CUSTOM, false))
        model.setLandscapeCount(prefs.getString(LAND_COUNT, MainActivity.DEFAULT_NUMBER)?.toInt())
        model.setPortraitCount(prefs.getString(PORT_COUNT, MainActivity.DEFAULT_NUMBER)?.toInt())
        model.getTheme().observe(this, Observer { updateTheme(it) })
    }

    private fun updateTheme(mode: Int) {
        delegate.localNightMode = mode
    }
}
