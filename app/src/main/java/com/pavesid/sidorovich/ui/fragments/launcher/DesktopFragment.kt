package com.pavesid.sidorovich.ui.fragments.launcher

import android.Manifest
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.PopupMenu
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import com.pavesid.sidorovich.LauncherApp
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.adapter.DesktopAdapter
import com.pavesid.sidorovich.databinding.FragmentDesktopBinding
import com.pavesid.sidorovich.model.Empty
import com.pavesid.sidorovich.model.Web
import com.pavesid.sidorovich.service.ImageLoaderService
import com.pavesid.sidorovich.service.ImageLoaderService.Companion.IMAGE_NAME_ALL
import com.pavesid.sidorovich.service.ImageLoaderService.Companion.IMAGE_NAME_DESKTOP
import com.pavesid.sidorovich.ui.fragments.ItemTouchHelperCallback
import com.pavesid.sidorovich.ui.activities.MainActivity
import com.pavesid.sidorovich.ui.decoration.SpacingItemDecoration
import com.pavesid.sidorovich.utils.ImageSaver
import com.pavesid.sidorovich.viewmodels.AppViewModel
import com.pavesid.sidorovich.viewmodels.BaseViewModel

class DesktopFragment : BaseGridFragment() {

    private var _binding: FragmentDesktopBinding? = null
    private val binding
        get() = _binding!!

    private lateinit var myAdapter: DesktopAdapter

    private val viewModel: AppViewModel by activityViewModels()

    private var mUpdateImageBroadcastReceiver: BroadcastReceiver? = null

//    private var imageLoader: ImageLoader

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDesktopBinding.inflate(inflater, container, false)

        activityApp = activity as MainActivity

        setupRecycler()

        Log.d("M_desk_isDiff", "${viewModel.isDifferentPictures().value == true}")
        if (viewModel.getServicePeriod().value ?: 1L != 1L) {
            if (viewModel.isDifferentPictures().value == true) {
                setBackground(IMAGE_NAME_DESKTOP)
                Log.d("M_desk_isDiff", "$IMAGE_NAME_DESKTOP")
            } else {
                setBackground(IMAGE_NAME_ALL)
                Log.d("M_desk_isDiff", "$IMAGE_NAME_ALL")
            }
        } else {
            binding.desktopLayout.background = null
        }

        setupItemTouchHelper()

        mUpdateImageBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(
                context: Context,
                intent: Intent
            ) {
                val action = intent.action
                if (ImageLoaderService.BROADCAST_ACTION_UPDATE_IMAGE == action) {
                    when (intent.getStringExtra(ImageLoaderService.BROADCAST_PARAM_IMAGE)) {
                        IMAGE_NAME_DESKTOP -> setBackground(IMAGE_NAME_DESKTOP)
                        IMAGE_NAME_ALL -> setBackground(IMAGE_NAME_ALL)
                    }
                }
            }
        }

        activity?.registerReceiver(
            mUpdateImageBroadcastReceiver,
            IntentFilter(ImageLoaderService.BROADCAST_ACTION_UPDATE_IMAGE)
        )

        if (activityApp.prefs.getBoolean(BaseViewModel.HINT_DESKTOP, true)) {
            binding.hintDesktop.setOnClickListener {
                it.visibility = View.GONE
                activityApp.prefs.edit().apply {
                    putBoolean(BaseViewModel.HINT_DESKTOP, false)
                    apply()
                }
            }
        } else {
            binding.hintDesktop.visibility = View.GONE
        }

        return binding.root
    }

    private fun setBackground(fileName: String) {
        var bitmap = LauncherApp.cache?.getImageFromCache(fileName)
        if (bitmap == null) {
            bitmap =
                ImageSaver.instance?.loadImage(LauncherApp.applicationContext(), fileName)
            if (bitmap == null) {
                Toast.makeText(this.context, "Image not yet", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this.context, "Image from disk", Toast.LENGTH_SHORT).show()
                LauncherApp.cache?.addImageToCache(fileName, bitmap)
            }
        } else {
            Toast.makeText(this.context, "Image from cache", Toast.LENGTH_SHORT).show()
        }
        binding.desktopLayout.background = BitmapDrawable(resources, bitmap)
    }

    override fun onDestroyView() {
        _binding = null
        activityApp.actionMode?.finish()

        if (mUpdateImageBroadcastReceiver != null) {
            activity?.unregisterReceiver(mUpdateImageBroadcastReceiver)
            mUpdateImageBroadcastReceiver = null
        }
        super.onDestroyView()
    }

    private fun setupRecycler() {
        count = 4

        val width = getWidth()

        val viewWidth = (width - dpToPx * (count + 1)) / count

        myAdapter = DesktopAdapter(
            viewModel,
            activityApp,
            viewWidth
        )

        viewModel.getContact().observe(viewLifecycleOwner, Observer { list ->
            viewModel.addDesktop(list.filter { it.position != -1 })
        })

        viewModel.getWebs().observe(
            viewLifecycleOwner, Observer {
                viewModel.addDesktop(it)
            }
        )

        viewModel.getEmpties().observe(
            viewLifecycleOwner, Observer {
                viewModel.addDesktop(it)
            }
        )

        viewModel.getAppsDesktop().observe(
            viewLifecycleOwner, Observer {
                viewModel.addDesktop(it)
            }
        )

        viewModel.getDesktops().observe(
            viewLifecycleOwner, Observer {
                myAdapter.updateData(it.toList())
            }
        )

        setupFAB()

        binding.recyclerDesktop.apply {
            adapter = myAdapter
            val layoutManager = GridLayoutManager(this@DesktopFragment.context, count)
            this.layoutManager = layoutManager
            addItemDecoration(SpacingItemDecoration(dpToPx))

//            addOnScrollListener(object : RecyclerView.OnScrollListener() {
//                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                    super.onScrolled(recyclerView, dx, dy)
//
//                    myAdapter.scrollDirection = if (dy > 0) {
//                        DesktopAdapter.ScrollDirection.DOWN
//                    } else {
//                        DesktopAdapter.ScrollDirection.UP
//                    }
//                }
//            })
        }
    }

    private fun setupItemTouchHelper() {
        val itemTouchHelper = ItemTouchHelper(
            ItemTouchHelperCallback(
                myAdapter
            )
        )
        itemTouchHelper.attachToRecyclerView(binding.recyclerDesktop)
    }

    private fun setupFAB() {

        binding.fab.apply {
            setOnClickListener {
                val popupMenu = PopupMenu(activityApp, this)
                popupMenu.inflate(R.menu.popup_menu_desktop)

                popupMenu.setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.add_app -> {
                            activityApp.peekApp()
                            true
                        }
                        R.id.add_contact -> {
                            activityApp.checkPermission(
                                Manifest.permission.READ_CONTACTS,
                                MainActivity.READ_CONTACTS_PERMISSION_DESKTOP_CODE,
                                ""
                            )
                            true
                        }
                        R.id.add_web -> {
                            addWebsiteDialog(viewModel.desktopSize + 1)
                            true
                        }
                        R.id.add_empty -> {
                            viewModel.addEmpty(Empty(position = viewModel.desktopSize + 1))
                            true
                        }
                        else -> false
                    }
                }

                popupMenu.show()
            }
        }
    }

    private fun addWebsiteDialog(pos: Int) {
        val builder = AlertDialog.Builder(context)
        val v =
            View.inflate(context, R.layout.web_dialog, null)
        builder.setView(v)
        builder.setPositiveButton(R.string.yes) { _, _ ->
            val nameView =
                v.findViewById<EditText>(R.id.tiNameSite)
            val urlView =
                v.findViewById<EditText>(R.id.tiURL)
            val name = nameView.text.toString()
            val url = urlView.text.toString()
            if (name.isNotEmpty() && url.isNotEmpty()) {
                val web = Web(url, name, pos)
                viewModel.addWeb(web)
            }
        }
        builder.setNegativeButton(
            "Cancel"
        ) { dialog: DialogInterface, _: Int -> dialog.cancel() }
        builder.show()
    }
}
