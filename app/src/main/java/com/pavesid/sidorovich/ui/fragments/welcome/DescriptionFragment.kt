package com.pavesid.sidorovich.ui.fragments.welcome

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.databinding.FragmentDescriptionBinding

/**
 * A simple [Fragment] subclass.
 */
class DescriptionFragment : Fragment() {

    private var _binding: FragmentDescriptionBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding
        get() = _binding!!

    companion object {
        @JvmStatic
        fun newInstance() =
            DescriptionFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDescriptionBinding.inflate(inflater, container, false)
        val view = binding.root

        initViews()

        return view
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun initViews() {
//        binding.button.visibility = View.GONE
//        apply {
//            setOnClickListener {
//                fragmentManager?.beginTransaction()
//                    ?.replace(R.id.fragment_container,
//                        ThemeSelectionFragment.newInstance()
//                    )
//                    ?.addToBackStack("DescriptionFragment")
//                    ?.commit()
//                    ?: throw NullPointerException("Fragment Manager or smth else is null in DescriptionFragment")
//            }
//
//            text = getString(R.string.next)
//        }

        binding.description.text = getString(R.string.description)

        binding.name?.text = getString(R.string.app_name)
    }
}
