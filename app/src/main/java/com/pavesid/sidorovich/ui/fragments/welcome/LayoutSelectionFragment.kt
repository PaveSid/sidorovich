package com.pavesid.sidorovich.ui.fragments.welcome

import android.os.Bundle
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.pavesid.sidorovich.LauncherApp.Companion.applicationContext
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.databinding.FragmentLayoutSelectionBinding
import com.pavesid.sidorovich.ui.custom.CustomRadioGroup
import com.pavesid.sidorovich.ui.custom.RadioButtonListener
import com.pavesid.sidorovich.viewmodels.BaseViewModel
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.IS_CUSTOM
import com.pavesid.sidorovich.viewmodels.BaseViewModel.Companion.LAYOUT

class LayoutSelectionFragment : Fragment() {

    private var _binding: FragmentLayoutSelectionBinding? = null
    private val viewModel: BaseViewModel by activityViewModels()

    // This property is only valid between onCreateView and onDestroyView.
    private val binding
        get() = _binding!!

    private val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext())

    companion object {
        @JvmStatic
        fun newInstance() =
            LayoutSelectionFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLayoutSelectionBinding.inflate(inflater, container, false)
        val view = binding.root

        initViews()

        return view
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun initViews() {
        if (viewModel.getLayout().value == true) {
            binding.tightButton.isChecked = true
        } else {
            binding.standardButton.isChecked = true
        }

        viewModel.isCustom(false)
        prefs.edit().apply {
            putBoolean(IS_CUSTOM, true)
            apply()
        }

        binding.standardButton.text = getString(R.string.standard_layout)
        binding.tightButton.text = getString(R.string.tight_layout)

        binding.descriptionStandard.text = getString(R.string.standard_description)
        binding.descriptionTight.text = getString(R.string.tight_description)

        // Не ясно почему не работает для клика по RadioButton. Пришлось дописывать еще 2 строки

        CustomRadioGroup.setOnClickListener(object : RadioButtonListener {
            override fun onClick(view: View?) {
                when (view?.id) {
                    R.id.standardButton -> {
                        binding.tightButton.isChecked = false
                        prefs.edit().apply {
                            putBoolean(LAYOUT, false)
                            apply()
                        }
                    }
                    R.id.tightButton -> {
                        binding.standardButton.isChecked = false
                        prefs.edit().apply {
                            putBoolean(LAYOUT, true)
                            apply()
                        }
                    }
                }
            }
        })

        binding.standardButton.setOnClickListener {
            binding.tightButton.isChecked = false
            prefs.edit().apply {
                putBoolean(LAYOUT, false)
                apply()
            }
        }

        binding.tightButton.setOnClickListener {
            binding.standardButton.isChecked = false
            prefs.edit().apply {
                putBoolean(LAYOUT, true)
                apply()
            }
        }
    }
}
