package com.pavesid.sidorovich.ui.fragments.launcher

import android.graphics.Point
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.pavesid.sidorovich.extensions.dpToPx
import com.pavesid.sidorovich.ui.activities.MainActivity

open class BaseGridFragment : Fragment() {

    protected var count = 0

    protected lateinit var activityApp: MainActivity

    protected var dpToPx: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dpToPx = 16.dpToPx(resources)
    }

    protected fun getWidth(): Int {
        val display = activity?.windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        return size.x
    }

//    protected fun retrieveContactPhoto(context: Context, number: String?): Bitmap? {
//        val contentResolver: ContentResolver = context.contentResolver
//        var contactId: Long = 0
//        val uri = Uri.withAppendedPath(
//            ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
//            Uri.encode(number)
//        )
//        val projection = arrayOf(
//            ContactsContract.PhoneLookup.DISPLAY_NAME,
//            ContactsContract.PhoneLookup._ID
//        )
//        val cursor: Cursor? = contentResolver.query(
//            uri,
//            projection,
//            null,
//            null,
//            null
//        )
//        if (cursor != null) {
//            while (cursor.moveToNext()) {
//                contactId =
//                    cursor.getLong(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID))
//            }
//            cursor.close()
//        }
//
//        var photo: Bitmap? = getBitmapFromVectorDrawable(R.drawable.ic_account_box)
//
//        try {
//            val inputStream: InputStream? =
//                ContactsContract.Contacts.openContactPhotoInputStream(
//                    context.contentResolver,
//                    ContentUris.withAppendedId(
//                        ContactsContract.Contacts.CONTENT_URI,
//                        contactId
//                    )
//                )
//            if (inputStream != null) {
//                photo = BitmapFactory.decodeStream(inputStream)
//            }
//            assert(inputStream != null)
//            inputStream?.close()
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        return photo
//    }

//    private fun getBitmapFromVectorDrawable(@DrawableRes drawableId: Int): Bitmap? {
//        var drawable: Drawable? =
//            ContextCompat.getDrawable(activityApp, drawableId)
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP && drawable != null) {
//            drawable = DrawableCompat.wrap(drawable).mutate()
//        }
//        val bitmap = Bitmap.createBitmap(
//            drawable?.intrinsicWidth ?: 0,
//            drawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888
//        )
//        val canvas = Canvas(bitmap)
//        drawable?.setBounds(0, 0, canvas.width, canvas.height)
//        drawable?.draw(canvas)
//        return bitmap
//    }
}