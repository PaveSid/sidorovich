package com.pavesid.sidorovich.ui.fragments

import androidx.recyclerview.widget.RecyclerView

interface ItemTouchHelperListener {
    fun onItemMove(recyclerView: RecyclerView, fromPosition: Int, toPosition: Int): Boolean
    fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder)
}