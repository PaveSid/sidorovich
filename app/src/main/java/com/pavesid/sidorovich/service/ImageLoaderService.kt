package com.pavesid.sidorovich.service

import android.app.IntentService
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.pavesid.sidorovich.LauncherApp
import com.pavesid.sidorovich.R
import com.pavesid.sidorovich.ui.activities.MainActivity
import com.pavesid.sidorovich.utils.ImageSaver
import com.pavesid.sidorovich.utils.Utils
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.net.URL

class ImageLoaderService : IntentService("ImageLoaderService") {
    override fun onCreate() {
        super.onCreate()
        createNotificationChannel(MainActivity.CHANNEL_ID_3)
        val builder: NotificationCompat.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            NotificationCompat.Builder(this, MainActivity.CHANNEL_ID_3)
                .setContentTitle("Update Image")
                .setSmallIcon(R.drawable.ic_list)
                .setPriority(NotificationCompat.PRIORITY_LOW)
        } else {
            NotificationCompat.Builder(this, MainActivity.CHANNEL_ID_3)
                .setContentTitle("Update Image")
                .setSmallIcon(R.drawable.ic_list)
                .setPriority(NotificationCompat.PRIORITY_LOW)
        }

        val notification = builder.build()
        startForeground(777, notification)
        Log.d(TAG, "ImageLoaderService#onCreate()")
    }

    override fun onHandleIntent(intent: Intent?) {
        if (intent == null) {
            Log.d(
                TAG,
                "LoaderService#onHandleIntent() with null intent"
            )
            return
        }
        val action = intent.action
        Log.d(
            TAG,
            "LoaderService#onHandleIntent() with action = $action"
        )

        when (action) {
            SERVICE_ACTION_LOAD_IMAGE_ALL -> {
                intent.extras?.getString("service")?.let {
                    Log.d(TAG, it)
                    try {
                        val drawable = downloadBitmap(it)
                        LauncherApp.cache?.removeImageFromCache(IMAGE_NAME_ALL)
                        LauncherApp.cache?.addImageToCache(IMAGE_NAME_ALL, drawable)
                        ImageSaver.instance?.saveImage(applicationContext, drawable, IMAGE_NAME_ALL)
                        val broadcastIntent = Intent(BROADCAST_ACTION_UPDATE_IMAGE)
                        broadcastIntent.putExtra(BROADCAST_PARAM_IMAGE, IMAGE_NAME_ALL)
                        sendBroadcast(broadcastIntent)
                    } catch (e: IOException) {
                        Utils.metricaErrorNetwork(e)
                    }
                }
            }
            SERVICE_ACTION_LOAD_IMAGE_DIFFERENT -> {

                intent.extras?.getString("service")?.let {
                    Log.d(TAG, it)
                    try {
                        val drawableList = downloadBitmap(it)
                        val drawableGrid = downloadBitmap(it)
                        val drawableDesktop = downloadBitmap(it)

                        LauncherApp.cache?.removeImageFromCache(IMAGE_NAME_LIST)
                        LauncherApp.cache?.addImageToCache(IMAGE_NAME_LIST, drawableList)
                        ImageSaver.instance?.saveImage(
                            applicationContext,
                            drawableList,
                            IMAGE_NAME_LIST
                        )
                        val broadcastIntentList =
                            Intent(BROADCAST_ACTION_UPDATE_IMAGE)
                        broadcastIntentList.putExtra(BROADCAST_PARAM_IMAGE, IMAGE_NAME_LIST)
                        sendBroadcast(broadcastIntentList)

                        LauncherApp.cache?.removeImageFromCache(IMAGE_NAME_GRID)
                        LauncherApp.cache?.addImageToCache(IMAGE_NAME_GRID, drawableGrid)
                        ImageSaver.instance?.saveImage(
                            applicationContext,
                            drawableGrid,
                            IMAGE_NAME_GRID
                        )
                        val broadcastIntentGrid =
                            Intent(BROADCAST_ACTION_UPDATE_IMAGE)
                        broadcastIntentGrid.putExtra(BROADCAST_PARAM_IMAGE, IMAGE_NAME_GRID)
                        sendBroadcast(broadcastIntentGrid)

                        LauncherApp.cache?.removeImageFromCache(IMAGE_NAME_DESKTOP)
                        LauncherApp.cache?.addImageToCache(IMAGE_NAME_DESKTOP, drawableDesktop)
                        ImageSaver.instance?.saveImage(
                            applicationContext,
                            drawableDesktop,
                            IMAGE_NAME_DESKTOP
                        )
                        val broadcastIntentDesktop =
                            Intent(BROADCAST_ACTION_UPDATE_IMAGE)
                        broadcastIntentDesktop.putExtra(BROADCAST_PARAM_IMAGE, IMAGE_NAME_DESKTOP)
                        sendBroadcast(broadcastIntentDesktop)
                    } catch (e: IOException) {
                        Utils.metricaErrorNetwork(e)
                    }
                }
            }
        }
    }

    private fun downloadBitmap(it: String): Bitmap {
        val url = URL(it)
        val urlConnection = url.openConnection()
        urlConnection.useCaches = true
        val inputStream = urlConnection.getInputStream()
        val buffer = ByteArrayOutputStream()
        var nRead: Int
        val data = ByteArray(16384)
        while (inputStream.read(data, 0, data.size).also { nRead = it } != -1) {
            buffer.write(data, 0, nRead)
        }
        buffer.flush()
        inputStream.close()
        val bitmap = buffer.toByteArray()
        return BitmapFactory.decodeByteArray(bitmap, 0, bitmap.size)
    }

    override fun onDestroy() {
        Log.d(TAG, "ImageLoaderService#onDestroy()")
        stopForeground(true)
        super.onDestroy()
    }

    private fun createNotificationChannel(CHANNEL_ID: String) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name) + CHANNEL_ID
            val descriptionText = getString(R.string.channel_description) + CHANNEL_ID
            val importance = NotificationManager.IMPORTANCE_LOW
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    companion object {
        private const val TAG = "PaveSid"
        const val SERVICE_ACTION_LOAD_IMAGE_ALL =
            "com.pavesid.sidorovich.LOAD_IMAGE_ALL"
        const val SERVICE_ACTION_LOAD_IMAGE_DIFFERENT =
            "com.pavesid.sidorovich.LOAD_IMAGE_DIFFERENT"

        const val IMAGE_NAME_ALL = "image_all"
        const val IMAGE_NAME_LIST = "image_list"
        const val IMAGE_NAME_GRID = "image_grid"
        const val IMAGE_NAME_DESKTOP = "image_desk"
        const val BROADCAST_ACTION_UPDATE_IMAGE =
            "com.pavesid.sidorovich.UPDATE_IMAGE"
        const val BROADCAST_PARAM_IMAGE = "com.pavesid.sidorovich.IMAGE"
    }
}