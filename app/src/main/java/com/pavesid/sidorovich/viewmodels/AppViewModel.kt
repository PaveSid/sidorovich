package com.pavesid.sidorovich.viewmodels

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.pavesid.sidorovich.LauncherApp
import com.pavesid.sidorovich.data.entityDB.AppsRepository
import com.pavesid.sidorovich.extensions.mutableLiveData
import com.pavesid.sidorovich.model.App
import com.pavesid.sidorovich.model.PopularApp
import com.pavesid.sidorovich.model.AppEntity
import com.pavesid.sidorovich.model.Contact
import com.pavesid.sidorovich.model.Desktop
import com.pavesid.sidorovich.model.Empty
import com.pavesid.sidorovich.model.Web
import com.pavesid.sidorovich.utils.Utils.APPLICATIONS_HEADER
import com.pavesid.sidorovich.utils.Utils.POPULAR_HEADER
import com.pavesid.sidorovich.utils.Utils.SINGLE_CAT
import kotlinx.coroutines.launch
import java.util.Collections
import java.util.Date
import kotlin.Comparator

open class AppViewModel : PopularsViewModel() {

    companion object {
        const val SORT_DEFAULT = 0
        const val SORT_BY_TIME = 1
        const val SORT_ALPHABETICALLY = 2
        const val SORT_ALPHABETICALLY_REVERSE = 3
        const val SORT_COUNT = 4
    }

    private lateinit var appsRepository: AppsRepository

    private lateinit var appsEntity: MutableList<AppEntity>
    private lateinit var appsFavoritesEntity: MutableList<AppEntity>

    private var apps: MutableList<App> = mutableListOf()
    private var appsFavorites: MutableSet<App> = mutableSetOf()
    private var appsPopular: MutableList<App> = mutableListOf()
    private var appsAll: MutableList<App> = mutableListOf()
    private var appsDesktop: MutableList<App> = mutableListOf()

    private val appsFavoritesLiveData = MutableLiveData<List<App>>()
    private var appsAllLiveData: MutableLiveData<List<App>> = mutableLiveData()
    private var appsDesktopLiveData: MutableLiveData<List<App>> = mutableLiveData()

    private val favLiveData: MediatorLiveData<Set<Desktop>> = MediatorLiveData()
    private val fav: MutableSet<Desktop> = mutableSetOf()

    private var mode: Int? = 0
    private var key = false
    private var dataPop = mutableListOf<String>()

    var popupSize = 0

    init {
        viewModelScope.launch {
            appsRepository = AppsRepository(db.appsDao())

            appsEntity = appsRepository.loadItems().toMutableList()
            appsFavoritesEntity = appsRepository.loadFavorites().toMutableList()
        }

        favLiveData.addSource(appsFavoritesLiveData) {
            fav.addAll(it)
            addToFavorites()
        }

        favLiveData.addSource(getContact()) { list ->
            fav.addAll(list.filter { it.isFavorite != -1 })
            addToFavorites()
        }

        favLiveData.addSource(getEmptiesFavorites()) {
            fav.addAll(it)
            addToFavorites()
        }
    }

    fun getApps(): LiveData<List<App>> {
        return appsAllLiveData
    }

    private fun deleteAppFromDesktop(app: App) {
        appsDesktop.remove(app)
        app.entity?.position = -1
        viewModelScope.launch {
            appsRepository.updateItem(app.entity)
        }
    }

    fun addApp(app: App) {
        apps.add(app)
        viewModelScope.launch {
            appsRepository.addItem(app.entity)
        }

        setNewMode(mode)
        collectData()

        appsAllLiveData.value = appsAll
    }

    fun setApps(appsNew: List<App>?) {
        apps = appsNew?.toMutableList() ?: mutableListOf()
        for (app in apps) {
            var key = false
            for (entity in appsEntity) {
                if (app.packageName == entity.packageName) {
                    app.entity = entity
                    if ((app.entity?.position ?: -1) != -1) {
                        appsDesktop.add(app)
                    }
                    key = true
                    break
                }
            }
            for (entity in appsFavoritesEntity) {
                if (app.packageName == entity.packageName) {
                    appsFavorites.add(app)
                    break
                }
            }
            if (!key) {
                val itemEntity = AppEntity(
                    app.packageName,
                    app.name
                )
                appsEntity.add(itemEntity)
                viewModelScope.launch {
                    appsRepository.addItem(
                        itemEntity
                    )
                }
            }
        }

        appsDesktopLiveData.value = appsDesktop

        if (key) {
            appsPopular.clear()

            for (item in dataPop) {
                for (app in apps) {
                    if (item == app.packageName) {
                        appsPopular.add(app)
                        break
                    }
                }
            }

            collectData()

            appsAllLiveData.value = appsAll
            key = false
            dataPop.clear()
        } else {

            collectData()

            appsAllLiveData.value = appsAll
        }
        appsFavoritesLiveData.value = appsFavorites.toList()
    }

    fun updateData(namePackage: String?) {
        for (app in appsAll) {
            if (app.packageName == namePackage) {
                val emptyP = Empty(position = app.entity?.position ?: -1)
                val emptyF = Empty(isFavorite = app.entity?.isFavorite ?: -1)
                desktops.remove(app)
                desktops.add(emptyP)
                changeDesktop()
                fav.remove(app)
                fav.add(emptyF)
                addToFavorites()
                deleteItem(app)
                viewModelScope.launch {
                    emptyRepository.addEmpty(emptyF)
                    emptyRepository.addEmpty(emptyP)
                }
                break
            }
        }
    }

    fun modifyItem(app: App) {
        val position = apps.indexOf(app)
        apps[position].entity?.count = apps[position].entity?.count?.plus(1) ?: 0
        apps[position].entity?.last = Date().time
        viewModelScope.launch {
            appsRepository.updateItem(apps[position].entity)
        }
        if (mode == SORT_COUNT) {
            setNewMode(SORT_COUNT)
        } else {
            collectData()

            appsAllLiveData.value = appsAll
        }
    }

    fun addToDesktop(app: App) {
        val position = apps.indexOf(app)
        apps[position].entity?.position = desktopSize + 1
        viewModelScope.launch {
            appsRepository.updateItem(apps[position].entity)
        }
        appsDesktop.add(apps[position])

        appsDesktopLiveData.value = appsDesktop
    }

    fun deleteFromDesktop(desktop: Desktop) {
        val position = desktop.getDesktopPosition()
        desktops.remove(desktop)
        viewModelScope.launch {
            emptyRepository.deleteEmpty(desktop as? Empty)
        }
        desktops.forEach {
            val pos = it.getDesktopPosition()
            if (pos > position) {
                it.updatePosition(pos - 1, this)
            }
        }
        changeDesktop()
    }

    fun getAppsDesktop(): LiveData<List<App>> = appsDesktopLiveData

    fun setNewMode(newMode: Int?) {
        mode = newMode
        when (mode) {
            SORT_DEFAULT -> Collections.sort(apps,
                Comparator { o1, o2 -> return@Comparator ((o2.time - o1.time)).toInt() }) // Compare by hours (It default)
            SORT_BY_TIME -> Collections.sort(apps,
                Comparator { o1, o2 -> return@Comparator ((o1.time - o2.time)).toInt() }) // Compare by hours
            SORT_ALPHABETICALLY -> Collections.sort(apps,
                Comparator { o1, o2 -> return@Comparator o1.name.compareTo(o2.name) }) // Compare by A-Z
            SORT_ALPHABETICALLY_REVERSE -> Collections.sort(apps,
                Comparator { o1, o2 -> return@Comparator o2.name.compareTo(o1.name) }) // Compare by Z-A
            SORT_COUNT -> Collections.sort(apps,
                Comparator { o1, o2 ->
                    return@Comparator (o2.entity?.count ?: 0) - (o1.entity?.count
                        ?: 0) // Compare by count
                })
        }
        collectData()

        appsAllLiveData.value = appsAll
    }

    private fun deleteItem(app: App) {
        viewModelScope.launch {
            appsRepository.deleteItem(app.entity)
        }
        apps.remove(app)
        appsPopular.remove(app)

        if (appsFavorites.remove(app)) {
            appsFavoritesLiveData.value = appsFavorites.toList()
        }

        if (mode == SORT_COUNT) {
            setNewMode(SORT_COUNT)
        } else {
            collectData()

            appsAllLiveData.value = appsAll
        }
    }

    private fun collectData() {
        popupSize = appsPopular.size

        appsAll.clear()
        appsAll.add(App(type = POPULAR_HEADER))
        if (popupSize == 0) {
            appsAll.add(App(type = SINGLE_CAT))
            popupSize = 1
        } else {
            appsAll.addAll(appsPopular)
        }
        appsAll.add(App(type = APPLICATIONS_HEADER))
        appsAll.addAll(apps)
    }

    fun getMode() = mode

    fun setDataLaunch(applicationList: List<PopularApp>, countPopular: Int) {

        val groupData = applicationList.groupBy { it.packageName }

        val count = if (groupData.size > countPopular) {
            countPopular
        } else {
            groupData.size
        }

        val data: List<String> =
            groupData.entries.sortedByDescending { it.value.size }.subList(0, count).map { it.key }

        if (apps.isEmpty()) {
            key = true
            dataPop.clear()
            dataPop.addAll(data)
        } else {

            appsPopular.clear()

            for (item in data) {
                for (app in apps) {
                    if (item == app.packageName) {
                        appsPopular.add(app)
                        break
                    }
                }
            }
            collectData()

            appsAllLiveData.value = appsAll
        }
    }

    //Desktop
    private val desktops: MutableSet<Desktop> = mutableSetOf()

    private val desktopsLiveData: MutableLiveData<Set<Desktop>> = mutableLiveData()

    var desktopSize = -1

    fun addDesktop(apps: List<Desktop>) {
        desktops.addAll(apps)
        changeDesktop()
    }

    fun getDesktops(): LiveData<Set<Desktop>> {
        desktopSize = desktops.size - 1
        return desktopsLiveData
    }

    fun cleanWeb(web: Web) {
        val pos = web.position
        val empty = Empty(pos)
        desktops.remove(web)
        desktops.add(empty)
        deleteWeb(web)
        addEmpty(empty)
        empties.add(empty)
        changeDesktop()
    }

    fun cleanContact(contact: Contact) {
        val pos = contact.position
        val empty = Empty(pos)
        desktops.remove(contact)
        desktops.add(empty)
        deleteContactFromDesktop(contact)
        addEmpty(empty)
        empties.add(empty)
        changeDesktop()
    }

    fun addContactToDesktop(number: String, name: String, position: Int) {
        val contact = contacts.filter { it.number == number }
        val newContact: Contact?
        if (contact.isEmpty()) {
            newContact = Contact(number, name, -1, position)
            viewModelScope.launch {
                contactRepository.addNumber(newContact)
            }
            desktops.add(newContact)

            changeDesktop()
        } else {
            newContact = contact[0]
            if (newContact.position == -1) {
                newContact.position = position
                viewModelScope.launch {
                    contactRepository.updateNumber(newContact)
                }

                desktops.add(newContact)

                changeDesktop()
            }
        }
//        desktops.remove(list[0])
    }

    fun cleanApp(app: App) {
        val pos = app.entity?.position ?: -1
        val empty = Empty(pos)
        desktops.remove(app)
        desktops.add(empty)
        deleteAppFromDesktop(app)
        addEmpty(empty)
        empties.add(empty)
        changeDesktop()
    }

    fun changeDesktop() {
        desktopSize = desktops.size - 1
        desktopsLiveData.value = desktops.toSortedSet(Comparator { o1, o2 ->
            (o1?.getDesktopPosition() ?: -1) - (o2?.getDesktopPosition() ?: -1)
        })
    }

    fun updatePositionApp(app: App, position: Int) {
        val index = apps.indexOf(app)
        apps[index].entity?.position = position
        viewModelScope.launch {
            appsRepository.updateItem(app.entity)
        }
    }

    //Favorites

    fun addAppToFavorites(app: App, favorites: Int) {
        val position = apps.indexOf(app)

        if (app.entity?.isFavorite == -1) {
            val list = fav.filter { it.getFavoritesPosition() == favorites }

            apps[position].entity?.isFavorite = favorites

            appsFavorites.add(apps[position])

            viewModelScope.launch {
                appsRepository.updateItem(apps[position].entity)
                emptyRepository.deleteEmpty(list[0] as? Empty)
            }

            fav.remove(list[0])
            fav.add(apps[position])

            addToFavorites()
        } else {
            Toast.makeText(LauncherApp.applicationContext(), "Это приложение уже в избранном", Toast.LENGTH_SHORT).show()
        }
    }

    fun cleanAppFromFavorites(app: App) {
        val position = apps.indexOf(app)
        fav.remove(app)
        val empty = Empty(isFavorite = app.entity?.isFavorite ?: -1)

        apps[position].entity?.isFavorite = -1

        appsFavorites.remove(app)

        viewModelScope.launch {
            appsRepository.updateItem(apps[position].entity)
            emptyRepository.addEmpty(empty)
        }

        fav.add(empty)

        addToFavorites()
    }

    fun addContactToFavorites(number: String, name: String, favorites: Int) {
        val contact = contacts.filter { it.number == number }
        val list = fav.filter { it.getFavoritesPosition() == favorites }
        val newContact: Contact?
        if (contact.isEmpty()) {
            newContact = Contact(number, name, favorites, -1)
            viewModelScope.launch {
                contactRepository.addNumber(newContact)
                emptyRepository.deleteEmpty(list[0] as? Empty)
            }
            fav.remove(list[0])
            fav.add(newContact)
            contacts.add(newContact)

            addToFavorites()
        } else {
            newContact = contact[0]
            val index = contacts.indexOf(newContact)
            if (contacts[index].isFavorite == -1) {
                contacts[index].isFavorite = favorites
                viewModelScope.launch {
                    contactRepository.updateNumber(contacts[index])
                    emptyRepository.deleteEmpty(list[0] as? Empty)
                }
                fav.remove(list[0])
                fav.add(contacts[index])
                addToFavorites()
            }
        }
    }

    fun cleanContactFromFavorites(contact: Contact) {
        val position = contact.isFavorite
        fav.remove(contact)

        val empty = Empty(isFavorite = position)
        if (contact.position != -1) {
            viewModelScope.launch {
                contact.isFavorite = -1
                contactRepository.updateNumber(contact)
                emptyRepository.addEmpty(empty)
            }
            val index = contacts.indexOf(contact)
            contacts[index].isFavorite = -1
        } else {
            viewModelScope.launch {
                contactRepository.deleteNumber(contact)
                emptyRepository.addEmpty(empty)
            }
            contacts.remove(contact)
        }

        fav.add(empty)

        addToFavorites()
    }

    private fun addToFavorites() {
        favLiveData.value = fav.toSortedSet(Comparator { o1, o2 ->
            (o1?.getFavoritesPosition() ?: -1) - (o2?.getFavoritesPosition() ?: -1)
        })
    }

    fun getFavorites(): LiveData<Set<Desktop>> {
        return favLiveData
    }
}