package com.pavesid.sidorovich.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pavesid.sidorovich.LauncherApp
import com.pavesid.sidorovich.data.MyRoomDatabase
import com.pavesid.sidorovich.data.userDB.UserRepository
import com.pavesid.sidorovich.extensions.mutableLiveData
import com.pavesid.sidorovich.model.User
import kotlinx.coroutines.launch

open class UserViewModel : BaseViewModel() {
    private lateinit var userRepository: UserRepository

    private var userLiveData = MutableLiveData<User>()

    val db = MyRoomDatabase.getDatabase(LauncherApp.applicationContext())

    init {
        viewModelScope.launch {
            userRepository = UserRepository(db.userDao())

            userLiveData = mutableLiveData(userRepository.loadUser())
        }
    }

    fun getUser(): LiveData<User?> {
        return userLiveData
    }

    fun updateUser(user: User) {
        this.userLiveData.value = user
        viewModelScope.launch {
            userRepository.insertUser(user)
        }
    }
}