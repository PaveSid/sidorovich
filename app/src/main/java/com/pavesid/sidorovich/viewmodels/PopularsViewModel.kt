package com.pavesid.sidorovich.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pavesid.sidorovich.data.popularDB.PopularRepository
import com.pavesid.sidorovich.extensions.mutableLiveData
import com.pavesid.sidorovich.model.App
import com.pavesid.sidorovich.model.PopularApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import java.util.Collections
import java.util.Date

open class PopularsViewModel : ContactViewModel() {

    private lateinit var popularRepository: PopularRepository

    private var appsPopularLiveData: MutableLiveData<List<PopularApp>> = mutableLiveData()

//    private var appsPopular: MutableList<App> = mutableListOf()

    init {
        viewModelScope.launch {
            popularRepository = PopularRepository(db.popularDao())
        }
    }

    @ObsoleteCoroutinesApi
    fun setPopulars(offset: Long): Job {
        return CoroutineScope(Dispatchers.IO).launch {
            val tickerChannel = ticker(delayMillis = 1_000, initialDelayMillis = 0)
            var populars: List<PopularApp> = Collections.emptyList()
            while (isActive) {
                tickerChannel.receive()
                val time = Date().time - offset
                val newPopulars = popularRepository.loadPopulars(time)
                if (populars != newPopulars) {
                    CoroutineScope(Dispatchers.Main).launch {
                        appsPopularLiveData.value = newPopulars
                    }
                    populars = newPopulars
                }
            }
        }
    }

    fun getPopulars(): LiveData<List<PopularApp>> = appsPopularLiveData

    fun addPopular(app: App) {
        val popularApp = PopularApp(app.packageName, Date().time)
        viewModelScope.launch {
            popularRepository.addPopular(popularApp)
        }
    }

    fun deletePopular(packageName: String?) {
        viewModelScope.launch {
            popularRepository.deletePopular(packageName)
        }
    }
}