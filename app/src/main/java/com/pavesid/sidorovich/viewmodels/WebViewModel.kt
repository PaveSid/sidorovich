package com.pavesid.sidorovich.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pavesid.sidorovich.data.webDB.WebRepository
import com.pavesid.sidorovich.extensions.mutableLiveData
import com.pavesid.sidorovich.model.Web
import kotlinx.coroutines.launch

open class WebViewModel : EmptyViewModel() {

    private lateinit var webRepository: WebRepository

    private var webs: MutableList<Web> = mutableListOf()

    private val websLiveData: MutableLiveData<List<Web>> = mutableLiveData()

    init {
        viewModelScope.launch {
            webRepository = WebRepository(db.webDao())

            webs = webRepository.loadWebs().toMutableList()
            websLiveData.value = webs
        }
    }

    fun getWebs(): LiveData<List<Web>> {
        return websLiveData
    }

    fun addWeb(web: Web) {
        if (!webs.contains(web)) {
            webs.add(web)
            viewModelScope.launch {
                webRepository.insertWeb(web)
            }
            websLiveData.value = webs
        }
    }

    fun deleteWeb(web: Web) {
        webs.remove(web)
        viewModelScope.launch {
            webRepository.deleteWeb(web)
        }
        websLiveData.value = webs
    }

    fun updateWeb(web: Web) {
//        val pos = webs.indexOf(web)
//        webs[pos].position = position
        viewModelScope.launch {
            webRepository.updateWeb(web)
        }
//        websLiveData.value = webs
    }

    fun updatePositionWeb(web: Web, position: Int) {
        val index = webs.indexOf(web)
        webs[index].position = position
        viewModelScope.launch {
            webRepository.updateWeb(webs[index])
        }
    }
}