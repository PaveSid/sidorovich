package com.pavesid.sidorovich.viewmodels

import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pavesid.sidorovich.extensions.mutableLiveData
import com.pavesid.sidorovich.ui.activities.MainActivity

open class BaseViewModel : ViewModel() {

    companion object {

        const val THEME = "switchTheme"
        const val LAYOUT = "switchLayout"
        const val IS_CUSTOM = "isCustomLayout"
        const val LAND_COUNT = "landscapeCount"
        const val PORT_COUNT = "portraitCount"
        const val START_WELCOME = "startFromWelcome"
        const val MODE = "sortMode"
        const val SHOW_FAVORITES = "showFavorites"
        const val HIDE_FAVORITES = "hideFavorites"
        const val SHOW_POPULARS = "showPopulars"
        const val PERIOD = "period"
        const val COUNT_POPULAR = "countPopular"
        const val SERVICE = "service"
        const val PERIOD_SERVICE = "periodService"
        const val DIFFERENT_PICTURES = "differentPicture"

        const val HINT_DESKTOP = "desk_hint"
        const val HINT_PROFILE = "profile_hint"
    }

    private val appTheme = MutableLiveData<Int>()
    private val appLayout = MutableLiveData<Boolean>()
    private val custom = MutableLiveData<Boolean>()
    private val landscapeCount = MutableLiveData<Int>()
    private val portraitCount = MutableLiveData<Int>()
    private val showFavorites = MutableLiveData<Boolean>()
    private val hideFavorites = MutableLiveData<Boolean>()
    private val showPopulars = MutableLiveData<Boolean>()
    private val period = MutableLiveData<Long>()
    private val countPopular = MutableLiveData<Int>()
    private val service = MutableLiveData<String>()
    private val servicePeriod = MutableLiveData<Long>()
    private val isDifferentPictures = MutableLiveData<Boolean>()

    private val needUpdateService = MutableLiveData<Boolean>()

//    private var periodService: Long = 0L
//    private var urlService: Int = 0

    private val servicePeriodLD = mutableLiveData(0L)
    private val urlServiceLD = mutableLiveData(0)

    fun switchTheme(isDark: Boolean) {
        if (isDark) {
            appTheme.value = AppCompatDelegate.MODE_NIGHT_YES
        } else {
            appTheme.value = AppCompatDelegate.MODE_NIGHT_NO
        }
    }

    fun updateUrlService(urlService: Int) {
        if (urlServiceLD.value != urlService) {
            urlServiceLD.value = urlService
            needUpdateService.value = true
        }
    }

    fun isNeedUpdate(): LiveData<Boolean> = needUpdateService

    fun updatePeriodService(periodService: Long) {
        if (servicePeriodLD.value != periodService) {
            servicePeriodLD.value = periodService
            needUpdateService.value = true
        }
    }

    fun switchLayout(isTight: Boolean) {
        appLayout.value = isTight
    }

    fun isCustom(isCustom: Boolean) {
        custom.value = isCustom
    }

    fun setLandscapeCount(count: Int?) {
        landscapeCount.value = count ?: MainActivity.DEFAULT_NUMBER.toInt()
    }

    fun setPortraitCount(count: Int?) {
        portraitCount.value = count ?: MainActivity.DEFAULT_NUMBER.toInt()
    }

    fun isShowFavorites(isShowFavorites: Boolean) {
        showFavorites.value = isShowFavorites
    }

    fun isHideFavorites(isHideFavorites: Boolean) {
        hideFavorites.value = isHideFavorites
    }

    fun isShowPopulars(isShowPopular: Boolean) {
        showPopulars.value = isShowPopular
    }

    fun isDifferentPictures(isDifferentPictures: Boolean) {
        this.isDifferentPictures.value = isDifferentPictures
    }

    fun setPeriod(periodData: Long) {
        period.value = periodData
    }

    fun setServicePeriod(period: Long) {
        servicePeriod.value = period
    }

    fun setCountPopular(countPopularData: Int) {
        countPopular.value = countPopularData
    }

    fun setService(service: Int, height: Int, width: Int) {
        val string = when (service) {
            1 -> "https://loremflickr.com/$width/$height"
            2 -> "https://picsum.photos/$width/$height"
            3 -> "https://source.unsplash.com/random/${width}x$height"
            4 -> "https://placeimg.com/$width/$height/any"
            else -> "https://www.random.org/bitmaps/?format=png&width=200&height=300&zoom=1"
        }
        this.service.value = string
    }

    fun getTheme(): LiveData<Int> = appTheme

    fun getLayout(): LiveData<Boolean> = appLayout

    fun isCustom(): LiveData<Boolean> = custom

    fun getLandscapeCount(): LiveData<Int> = landscapeCount

    fun getPortraitCount(): LiveData<Int> = portraitCount

    fun isShowFavorites(): LiveData<Boolean> = showFavorites

    fun isHideFavorites(): LiveData<Boolean> = hideFavorites

    fun isShowPopulars(): LiveData<Boolean> = showPopulars

    fun isDifferentPictures(): LiveData<Boolean> = isDifferentPictures

    fun getPeriod(): LiveData<Long> = period

    fun getCountPopular(): LiveData<Int> = countPopular

    fun getService(): LiveData<String> = service

    fun getServicePeriod(): LiveData<Long> = servicePeriod
}