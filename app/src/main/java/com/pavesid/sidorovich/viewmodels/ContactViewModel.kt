package com.pavesid.sidorovich.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pavesid.sidorovich.data.contactDB.ContactRepository
import com.pavesid.sidorovich.extensions.mutableLiveData
import com.pavesid.sidorovich.model.Contact
import kotlinx.coroutines.launch

open class ContactViewModel : WebViewModel() {
    protected lateinit var contactRepository: ContactRepository

    private var contactFavoritesLiveData: MutableLiveData<List<Contact>> = mutableLiveData()
    private var contactDesktopsLiveData: MutableLiveData<List<Contact>> = mutableLiveData()
    private var contactsLiveData: MutableLiveData<List<Contact>> = mutableLiveData()
    protected var contacts: MutableList<Contact> = mutableListOf()

    init {
        viewModelScope.launch {
            contactRepository = ContactRepository(db.contactDao())

            contacts = contactRepository.loadContacts().toMutableList()

            contactsLiveData.value = contacts
        }
    }

    fun getContact(): LiveData<List<Contact>> {
        return contactsLiveData
    }

//    fun getFavoritesContact(): LiveData<List<Contact>> {
//        return contactFavoritesLiveData
//    }

//    fun getDesktopsContact(): LiveData<List<Contact>> {
//        return contactDesktopsLiveData
//    }

//    fun setFavoritesContact(list: List<Contact>) {
//        contactsFavorites = list.toMutableList()
//        contactFavoritesLiveData.value = list
//    }

//    fun updateContact(number: String, name: String, favorites: Int, position: Int) {
//        val contact = contactsFavorites.filter { it.number == number }
//        if (contact.isEmpty()) {
//            viewModelScope.launch {
//                contactRepository.addNumber(
//                    Contact(number, name, favorites, position)
//                )
//            }
//        } else {
//            contact[0]
//        }
//
//    }

//    fun deleteContact(number: Contact) {
//        viewModelScope.launch {
//            contactRepository.deleteNumber(number)
//        }
//    }

    fun updatePositionContact(contact: Contact, position: Int) {
        val index = contacts.indexOf(contact)

        contacts[index].position = position
        viewModelScope.launch {
            contactRepository.updateNumber(contacts[index])
        }
    }

    fun deleteContactFromDesktop(contact: Contact) {
        val index = contacts.indexOf(contact)

        if (contacts[index].isFavorite != -1) {
            contacts[index].position = -1
            viewModelScope.launch {
                contactRepository.updateNumber(contacts[index])
            }
        } else {
            viewModelScope.launch {
                contactRepository.deleteNumber(contacts[index])
            }
            contacts.remove(contact)
//            contacts.remove(contact)
//            contactLiveData.value = contacts
        }
        contactsLiveData.value = contacts
//        contactsDesktops.remove(contact)
//        contactDesktopsLiveData.value = contactsDesktops
    }

//    fun addContactToDesktop(number: String, name: String, position: Int) {
//        val contact = contacts.filter { it.number == number }
//        val newContact: Contact?
//        if (contact.isEmpty()) {
//            newContact = Contact(number, name, favorites, -1)
//            viewModelScope.launch {
//                contactRepository.addNumber(newContact)
//                emptyRepository.deleteEmpty(list[0] as? Empty)
//            }
//        } else {
//            newContact = contact[0]
//            newContact.isFavorite = favorites
//            viewModelScope.launch {
//                contactRepository.updateNumber(newContact)
//                emptyRepository.deleteEmpty(list[0] as? Empty)
//            }
//        }
//    }
}