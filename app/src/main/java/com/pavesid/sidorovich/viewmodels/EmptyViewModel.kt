package com.pavesid.sidorovich.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pavesid.sidorovich.data.emptyDB.EmptyRepository
import com.pavesid.sidorovich.extensions.mutableLiveData
import com.pavesid.sidorovich.model.Empty
import kotlinx.coroutines.launch

open class EmptyViewModel : UserViewModel() {
    protected lateinit var emptyRepository: EmptyRepository

    protected var empties: MutableList<Empty> = mutableListOf()
    private var emptiesFavorites: MutableList<Empty> = mutableListOf()

    private var emptyLiveData: MutableLiveData<List<Empty>> = mutableLiveData()
    private var emptyFavoritesLiveData: MutableLiveData<List<Empty>> = mutableLiveData()

    init {
        viewModelScope.launch {
            emptyRepository = EmptyRepository(db.emptyDao())

            empties = emptyRepository.loadEmpties().toMutableList()
            emptyLiveData.value = empties

            emptiesFavorites = emptyRepository.loadEmptiesFavorites().toMutableList()
            emptyFavoritesLiveData.value = emptiesFavorites
        }
    }

    fun getEmpties(): LiveData<List<Empty>> = emptyLiveData

    fun getEmptiesFavorites(): LiveData<List<Empty>> = emptyFavoritesLiveData

    fun addEmpty(empty: Empty) {
        viewModelScope.launch {
            emptyRepository.addEmpty(empty)
        }
        empties.add(empty)
        emptyLiveData.value = empties
    }

    fun deleteEmpty(empty: Empty) {
        viewModelScope.launch {
            emptyRepository.deleteEmpty(empty)
        }
        empties.remove(empty)
        emptyLiveData.value = empties
    }

    fun updateEmpty(empty: Empty) {
        viewModelScope.launch {
            emptyRepository.addEmpty(empty)
        }
//        empties.add(empty)
//        emptyLiveData.value = empties
    }

    fun updatePositionEmpty(empty: Empty, position: Int) {
        val index = empties.indexOf(empty)
        empties[index].position = position
        viewModelScope.launch {
            emptyRepository.updateEmpty(empties[index])
        }
    }
}