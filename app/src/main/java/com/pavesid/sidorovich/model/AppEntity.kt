package com.pavesid.sidorovich.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.pavesid.sidorovich.model.AppEntity.Companion.ENTITY_TABLE_NAME

@Entity(tableName = ENTITY_TABLE_NAME)
data class AppEntity(
    @PrimaryKey
    @ColumnInfo (name = PACKAGE_NAME)
    val packageName: String,
    @ColumnInfo (name = NAME)
    var name: String = "",
    @ColumnInfo (name = LAST)
    var last: Long = 0L,
    @ColumnInfo (name = COUNT)
    var count: Int = 0,
    @ColumnInfo (name = IS_FAVORITE)
    var isFavorite: Int = -1,
    @ColumnInfo (name = POSITION)
    var position: Int = -1
) {
    companion object {
        const val ENTITY_TABLE_NAME = "entities"

        const val PACKAGE_NAME = "packageName"
        const val NAME = "name"
        const val LAST = "last"
        const val COUNT = "count"
        const val IS_FAVORITE = "isFavorite"
        const val POSITION = "position"
    }
    // In API 23 was "java.lang.NoSuchMethodError: No static method hashCode(I)I in class Ljava/lang/Integer" without this
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AppEntity

        if (packageName != other.packageName) return false
        if (count != other.count) return false

        return true
    }

    override fun hashCode(): Int {
        var result = packageName.hashCode()
        result = 31 * result + count.toString().hashCode()
        return result
    }
}