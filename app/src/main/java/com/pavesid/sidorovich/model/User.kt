package com.pavesid.sidorovich.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.pavesid.sidorovich.model.User.Companion.USER_TABLE_NAME

@Entity(tableName = USER_TABLE_NAME)
data class User(
    @ColumnInfo(name = MOBILE)
    var mobile: String = "",
    @ColumnInfo(name = WORK_PHONE)
    var workPhone: String = "",
    @ColumnInfo(name = PERSONAL_MAIL)
    var personalMail: String = "",
    @ColumnInfo(name = WORK_MAIL)
    var workMail: String = "",
    @ColumnInfo(name = PERSONAL_ADDRESS)
    var personalAddress: String = "",
    @ColumnInfo(name = WORK_ADDRESS)
    var workAddress: String = "",
    @ColumnInfo(name = GITLAB)
    var gitlab: String = "",
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0
) {
    companion object {
        const val USER_TABLE_NAME = "user"

        const val MOBILE = "mobile"
        const val WORK_PHONE = "workPhone"
        const val PERSONAL_MAIL = "personalMail"
        const val WORK_MAIL = "workMail"
        const val PERSONAL_ADDRESS = "personalAddress"
        const val WORK_ADDRESS = "workAddress"
        const val GITLAB = "gitlab"
    }
}