package com.pavesid.sidorovich.model

import com.pavesid.sidorovich.viewmodels.AppViewModel

interface Desktop {
    fun getDesktopName(): String
    fun getDesktopType(): Int
    fun setDesktopPosition(position: Int)
    fun getDesktopPosition(): Int
    fun setFavoritesPosition(position: Int)
    fun getFavoritesPosition(): Int
    fun updatePosition(position: Int, viewModel: AppViewModel)
}