package com.pavesid.sidorovich.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.pavesid.sidorovich.model.Web.Companion.WEB_TABLE_NAME
import com.pavesid.sidorovich.utils.Utils.WEB
import com.pavesid.sidorovich.viewmodels.AppViewModel

@Entity(tableName = WEB_TABLE_NAME)
data class Web (
    @PrimaryKey
    @ColumnInfo (name = URL)
    val url: String,
    @ColumnInfo(name = NAME)
    val name: String = "",
    @ColumnInfo(name = POSITION)
    var position: Int = -1,
    @ColumnInfo(name = TYPE)
    var type: Int = WEB
) : Desktop {
    companion object {
        const val WEB_TABLE_NAME = "web"

        const val URL = "url"
        const val NAME = "name"
        const val POSITION = "position"
        const val TYPE = "type"
    }

    override fun getDesktopName(): String {
        return name
    }

    override fun getDesktopType(): Int {
        return type
    }

    override fun setDesktopPosition(position: Int) {
        this.position = position
    }

    override fun getDesktopPosition(): Int {
        return position
    }

    override fun setFavoritesPosition(position: Int) { }

    override fun getFavoritesPosition(): Int = -1
    override fun updatePosition(position: Int, viewModel: AppViewModel) {
        viewModel.updatePositionWeb(this, position)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Web

        if (url != other.url) return false
        if (name != other.name) return false
        if (position != other.position) return false
        if (type != other.type) return false

        return true
    }

    override fun hashCode(): Int {
        var result = url.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + position
        result = 31 * result + type
        return result
    }
}