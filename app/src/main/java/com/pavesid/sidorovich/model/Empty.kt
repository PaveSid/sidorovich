package com.pavesid.sidorovich.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.pavesid.sidorovich.model.Empty.Companion.EMPTY_TABLE_NAME
import com.pavesid.sidorovich.utils.Utils.EMPTY
import com.pavesid.sidorovich.viewmodels.AppViewModel
import java.util.UUID

@Entity(tableName = EMPTY_TABLE_NAME)
data class Empty(
    @ColumnInfo(name = POSITION)
    var position: Int = -1,
    @ColumnInfo(name = TYPE)
    var type: Int = EMPTY,
    @ColumnInfo (name = AppEntity.IS_FAVORITE)
    var isFavorite: Int = -1,
    @PrimaryKey
    @ColumnInfo(name = KEY)
    val id: String = UUID.randomUUID().toString()
) : Desktop {

    companion object {
        const val EMPTY_TABLE_NAME = "empty"

        const val KEY = "key"
        const val IS_FAVORITE = "isFavorite"
        const val POSITION = "position"
        const val TYPE = "type"
    }

    override fun getDesktopName(): String = id//.toString()

    override fun getDesktopType(): Int = type

    override fun setDesktopPosition(position: Int) {
        this.position = position
    }

    override fun getDesktopPosition(): Int = position

    override fun setFavoritesPosition(position: Int) {
        isFavorite = position
    }

    override fun getFavoritesPosition(): Int = isFavorite

    override fun updatePosition(position: Int, viewModel: AppViewModel) {
//        val empty = this
//        empty.position = position
        viewModel.updatePositionEmpty(this, position)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Empty

        if (position != other.position) return false
        if (type != other.type) return false
        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        var result = position
        result = 31 * result + type
        result = 31 * result + id.hashCode()
        return result
    }
}