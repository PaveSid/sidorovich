package com.pavesid.sidorovich.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.pavesid.sidorovich.model.Contact.Companion.NUMBER_PHONE_TABLE_NAME
import com.pavesid.sidorovich.utils.Utils.CONTACT
import com.pavesid.sidorovich.viewmodels.AppViewModel

@Entity (tableName = NUMBER_PHONE_TABLE_NAME)
data class Contact (
    @PrimaryKey
    @ColumnInfo (name = NUMBER)
    val number: String,
    @ColumnInfo (name = NAME)
    val name: String = "",
    @ColumnInfo(name = IS_FAVORITE)
    var isFavorite: Int = -1,
    @ColumnInfo(name = POSITION)
    var position: Int = -1,
    @ColumnInfo(name = TYPE)
    var type: Int = CONTACT
) : Desktop {
    companion object {
        const val NUMBER_PHONE_TABLE_NAME = "favorite_number"

        const val NUMBER = "number"
        const val IS_FAVORITE = "isFavorite"
        const val NAME = "name"
        const val POSITION = "position"
        const val TYPE = "type"
    }

//    fun getIconUrl() : String {
//        return "https://favicon.yandex.net/favicon/$number?size=32"
//    }

    override fun getDesktopName(): String = name

    override fun getDesktopType(): Int = type

    override fun setDesktopPosition(position: Int) {
        this.position = position
    }

    override fun getDesktopPosition(): Int = position

    override fun setFavoritesPosition(position: Int) {
        isFavorite = position
    }

    override fun getFavoritesPosition(): Int = isFavorite

    override fun updatePosition(position: Int, viewModel: AppViewModel) {
//        val contact = this
//        contact.position = position
        viewModel.updatePositionContact(this, position)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Contact

        if (number != other.number) return false
        if (name != other.name) return false
        if (isFavorite != other.isFavorite) return false
        if (position != other.position) return false
        if (type != other.type) return false

        return true
    }

    override fun hashCode(): Int {
        var result = number.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + isFavorite
        result = 31 * result + position
        result = 31 * result + type
        return result
    }
}