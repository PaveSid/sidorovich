package com.pavesid.sidorovich.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.pavesid.sidorovich.model.PopularApp.Companion.POPULARS_TABLE_NAME

@Entity(tableName = POPULARS_TABLE_NAME)
data class PopularApp(
    @ColumnInfo (name = PACKAGE_NAME)
    val packageName: String,
    @PrimaryKey
    @ColumnInfo (name = DATE)
    val date: Long
) {

    companion object {
        const val POPULARS_TABLE_NAME = "populars"

        const val PACKAGE_NAME = "packageName"
        const val DATE = "date"
    }
}