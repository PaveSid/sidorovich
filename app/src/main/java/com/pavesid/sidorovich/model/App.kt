package com.pavesid.sidorovich.model

import android.graphics.drawable.Drawable
import com.pavesid.sidorovich.utils.Utils.APPLICATION
import com.pavesid.sidorovich.viewmodels.AppViewModel

data class App(
    var packageName: String = "",
    var name: String = "",
    var image: Drawable? = null,
    var time: Long = 0,
    var entity: AppEntity? = null,
    var type: Int = APPLICATION
) : Desktop {
    // In API 23 was "java.lang.NoSuchMethodError: No static method hashCode(I)I in class Ljava/lang/Integer" without this
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as App

        if (packageName != other.packageName) return false
        if (name != other.name) return false
        if (time != other.time) return false

        return true
    }

    override fun hashCode(): Int {
        var result = packageName.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + time.toString().hashCode()
        return result
    }

    override fun getDesktopName(): String = packageName

    override fun getDesktopType(): Int = type

    override fun setDesktopPosition(position: Int) {
        this.entity?.position = position
    }

    override fun getDesktopPosition(): Int = entity?.position ?: -1

    override fun setFavoritesPosition(position: Int) {
        this.entity?.isFavorite = position
    }

    override fun getFavoritesPosition(): Int = this.entity?.isFavorite ?: -1

    override fun updatePosition(position: Int, viewModel: AppViewModel) {
//        val appEntity = this.entity
//        appEntity?.position = position
//        if (appEntity != null) {
            viewModel.updatePositionApp(this, position)
//        }
    }
}