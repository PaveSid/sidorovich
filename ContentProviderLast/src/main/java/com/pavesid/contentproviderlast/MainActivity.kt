package com.pavesid.contentproviderlast

import android.content.ContentValues
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.packageN
import kotlinx.android.synthetic.main.activity_main.count
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private val APPS_URI = Uri.parse("content://com.pavesid.sidorovich.all_apps/apps")
    private val LAST_URI = Uri.parse("content://com.pavesid.sidorovich.all_apps/last")
    private val USER_URI = Uri.parse("content://com.pavesid.sidorovich.all_apps/user")

    private val PACKAGE_NAME = "packageName"
    private val NAME = "name"
    private val COUNT = "count"

    private val MOBILE = "mobile"
    private val WORK_PHONE = "workPhone"
    private val PERSONAL_MAIL = "personalMail"
    private val WORK_MAIL = "workMail"
    private val PERSONAL_ADDRESS = "personalAddress"
    private val WORK_ADDRESS = "workAddress"
    private val GITLAB = "gitlab"

    private lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView = findViewById(R.id.tv)

        findViewById<Button>(R.id.showAppsBtn).setOnClickListener {
            try {
                requestApps(APPS_URI)
            } catch (ex: SecurityException) {
                textView.text = "Нет прав на просмотр списка приложений"
            }

        }

        findViewById<Button>(R.id.showLastBtn).setOnClickListener {
            try {
                requestApps(LAST_URI)
            } catch (ex: SecurityException) {
                textView.text = "Нет прав на просмотр последнего запущенного приложения"
            }
        }

        findViewById<Button>(R.id.editCountBtn).setOnClickListener {
            try {
                textView.text = modifyCount(
                    APPS_URI,
                    packageN.text.toString(),
                    count.text.toString().toInt()
                ).toString()
            } catch (ex: SecurityException) {
                textView.text = "Нет прав на изменение"
            } catch (e: Exception) {
                textView.text = "Что-то не так, возможно count не число"
            }
        }

        findViewById<Button>(R.id.userInfoBtn).setOnClickListener {
            try {
                requestUser(USER_URI)
            } catch (ex: SecurityException) {
                textView.text = "Нет прав на просмотр информации о пользователе"
            }
        }
    }

    private fun requestApps(uri: Uri) {
        val cursor = contentResolver.query(uri, null, null, null, null)
        val stringBuilder = StringBuilder().append("Apps: ").append("\n")
        cursor?.use {
            while (it.moveToNext()) {
                val packageName = it.getString(it.getColumnIndex(PACKAGE_NAME))
                val name = it.getString(it.getColumnIndex(NAME))
                val count = it.getInt(it.getColumnIndex(COUNT))

                stringBuilder.append("apps name = ").append(name).append("; ")
                    .append("packageName = ").append(packageName).append("; ")
                    .append("count = ").append(count).append("\n").append("\n")
            }
        }
        cursor?.close()
        textView.text = stringBuilder.toString()
    }

    private fun requestUser(uri: Uri) {
        val cursor = contentResolver.query(uri, null, null, null, null)
        val stringBuilder = StringBuilder().append("User: ").append("\n")
        cursor?.use {
            while (it.moveToNext()) {
                val mobile = it.getString(it.getColumnIndex(MOBILE))
                val phone = it.getString(it.getColumnIndex(WORK_PHONE))
                val mail = it.getString(it.getColumnIndex(PERSONAL_MAIL))
                val workMail = it.getString(it.getColumnIndex(WORK_MAIL))
                val personalAdd = it.getString(it.getColumnIndex(PERSONAL_ADDRESS))
                val workAdd = it.getString(it.getColumnIndex(WORK_ADDRESS))
                val gitlab = it.getString(it.getColumnIndex(GITLAB))

                stringBuilder.append("mobile = ").append(mobile).append(";\n")
                    .append("phone = ").append(phone).append(";\n")
                    .append("mail = ").append(mail).append(";\n")
                    .append("workMail = ").append(workMail).append(";\n")
                    .append("personalAdd = ").append(personalAdd).append(";\n")
                    .append("workAdd = ").append(workAdd).append(";\n")
                    .append("gitlab = ").append(gitlab).append(";\n")
            }
        }
        cursor?.close()
        textView.text = stringBuilder.toString()
    }

    private fun modifyCount(uri: Uri, packageName: String, count: Int): Int {
        val entity = ContentValues()
        entity.put(PACKAGE_NAME, packageName)
        entity.put(COUNT, count)
        return contentResolver.update(uri, entity, null, null)
    }
}
