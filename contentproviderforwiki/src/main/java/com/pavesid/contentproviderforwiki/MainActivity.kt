package com.pavesid.contentproviderforwiki

import android.content.ContentValues
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val random = Random()

        val uri = Uri.withAppendedPath(
            Uri.Builder().scheme("content").authority("org.wikipedia").build(),
            "history/query"
        )

        showRecordsBtn.setOnClickListener {
            requestApps(uri)
        }

        addRecordBtn.setOnClickListener {
            add(uri, random.nextInt())
        }
    }

    private fun requestApps(uri: Uri) {
        val cursor = contentResolver.query(uri, null, null, null, null)
        val stringBuilder = StringBuilder().append("Search_history: ").append("\n")
        cursor?.use { cursor ->
            stringBuilder.append("Count of records: ").append(cursor.count).append("\n").append("\n")
            while (cursor.moveToNext()) {
                val id = cursor.getInt(cursor.getColumnIndex("_id"))
                val text = cursor.getString(cursor.getColumnIndex("text"))
                val time = cursor.getInt(cursor.getColumnIndex("timestamp"))
//
                stringBuilder.append("id = ").append(id).append("; ")
                    .append("text = ").append(text).append("; ")
                    .append("time = ").append(time).append("\n").append("\n")
            }
        }
        tv.text = stringBuilder.toString()
    }

    fun add(uri: Uri, id: Int) {
        val k = ContentValues()
        k.put("_id", id)
        k.put("text", "From PaveSid")
        k.put("timestamp", Date().time.toInt())
        contentResolver.insert(uri, k)
    }
}
